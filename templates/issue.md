_Originally created by {{redmine_issue.author|user}} on {{issue_link(redmine_issue)}}_

{{redmine_issue.description|textile2markdown}}

{% if custom_fields.16.value %}
Blueprint: {{custom_fields.16.value}}
{% endif %}

{% if custom_fields.11.value %}
Feature Branch: {{custom_fields.11.value}}
{% endif %}

{% if redmine_issue.attachments %}
### Attachments
{% for a in redmine_issue.attachments %}
  * {{a|attachment}}{% endfor %}
{% endif %}

{% if redmine_issue.parent %}
Parent Task: {{redmine_issue.parent.id|issue}}
{% endif %}

{% if has_subtasks(redmine_issue) %}
### Subtasks
{% for i in issue_subtasks(redmine_issue) %}
  - {% if i.closed_on %}[x]{% else %}[ ]{% endif %} {{i|issue}}{% endfor %}
{% endif %}

{% if issue_has_relations(redmine_issue) %}
### Related issues
{% for rel in issue_relations(redmine_issue) if issue_and_no_label(rel.other) %}
  - {% if rel.label == "Blocked by"%}{% if rel.other.closed_on %}[x]{% else %}[ ]{% endif %} {% endif %}**{{rel.label}}** {{rel.other|issue}}{% endfor %}
{% endif %}
