_Originally created by {{event.user|user}} on {{issue_link(redmine_issue,pos)}}_

{% if journal_has_details(event) %}
### Metadata changes
{% for detail in journal_details(event) %}
  * {{ detail.label }}{% endfor %}
{% endif %}

{% if event.notes %}
{{event.notes|textile2markdown}}
{% endif %}
