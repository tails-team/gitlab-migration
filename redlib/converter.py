import copy
import functools
import jinja2
import logging
import operator
import re
from typing import Dict, Any

from .redmine import Redmine
from .gitlab import GitlabProject
from .redmine_format import Formatter
from .settings import Settings
from .textileconverter import TextileConverter
from .templates import Templates


log = logging.getLogger(__name__)

CORE_WORK = {
    15396: "Debian",
    16209: "Foundations Team",
    13284: "Sysadmin",
    13242: "Sysadmin",
    17247: "Technical writing",
    17246: "User experience",
}

CUSTOM_FIELDS_LABEL_COLOR = {
    'Type of work': '#F00000',
    15: '#F00000',
    'Affected tool': '#8E44AD',
    18: '#8E44AD',
    'Deliverable for': '#0000F0',
    19: '#0000F0',
    'Starter': '#F0F0F0',
    17: '#F0F0F0',
}

CUSTOM_FIELDS_PREFIX = {
    'Type of work': "T:",
    15: 'T:',
    'Affected tool': "C:",
    18: 'C:',
    'Deliverable for': "D:",
    19: 'D:',
}

MIGRATION_USER = "import-from-Redmine"

def gitlab_status(redmine_issue):
    status = redmine_issue['status']['name']

    if status in ("Duplicate", "Rejected", "Resolved"):
        return "close"
    return "reopen"


def gitlab_status_labels(redmine_issue):
    status = redmine_issue['status']['name']

    if status == "Confirmed":
        return ["To Do",]
    elif status == "In Progress":
        return ["Doing",]
    elif status == "Needs Validation":
        return ["Needs Validation",]
    elif status == "Duplicate":
        return ["Duplicate",]
    elif status == "Rejected":
        return ["Rejected",]
    elif status not in ("Resolved", "New"):
        log.warning("%d: Unknown status %s", redmine_issue['id'], status)
    return []


class GitlabFormatter(Formatter):
    def __init__(self, redmine, gitlab, settings):
        super().__init__(redmine)
        self.gitlab = gitlab
        self.settings = settings

    def core_work_label(self, name):
        label = f"Core Work:{name}"
        self.gitlab.label_create_if_needed(label, "#D10069")
        return label

    def format_issue(self, issue, with_subject=False):
        if isinstance(issue, (int, str)):
            issue_nr = int(issue)
        else:
            issue_nr = int(issue['id'])

        if issue_nr in CORE_WORK:
            label = self.core_work_label(CORE_WORK[issue_nr])
            return f'~"{label}"'

        gitlab_issue = self.gitlab.issue(issue_nr)

        if gitlab_issue:
            if gitlab_issue.get('project', None) is None:
                log.warning(f'#{issue_nr} has no project set - that is weird.')
                self.gitlab.fix_issue(issue_nr, gitlab_issue['project_id'])
                gitlab_issue = self.gitlab.issue(issue_nr)
                if gitlab_issue.get('project', None) is None:
                    raise gitlab.APIException(f"could not fix the project entry for #{issue_nr}")

            return f"{gitlab_issue['project']}#{issue_nr}"

        return f"#{issue_nr}"

    def issue_and_no_label(self, issue):
        return not self.format_issue(issue).startswith("~")

    def format_user(self, user, default=lambda u: "Anonymous", markdown=True):
        user_super = super().format_user(user, default=default)
        if not markdown:
            return user_super
        try:
            user = "@"+super().format_user(user, default=lambda u:None)
            if user == "@Tails":
                return user_super
            return user
        except TypeError:
            return user_super

    def format_attachment(self, attachment):
        link = attachment['content_url']
        if link.startswith("http://redmine.tails.boum.org"):
            link = link.replace("http://", "https://", 1)
        return f"[{attachment['filename']}]({link})"

    def format_custom_field(self, cf: Dict[Any, Any], default=operator.itemgetter("value"), markdown=True):
        """
        cf is a dict containing at least {
            "id": custom field id
            "value": custom field value
        }
        """
        prefix = CUSTOM_FIELDS_PREFIX.get(cf['id'], "")
        cf_info = super().format_custom_field(cf, default=lambda i: None)
        try:
            label = prefix + cf_info
        except TypeError:
            return super().format_custom_field(cf, default=default)

        if cf['id'] == 17: # Starter
            if cf['value'] == '1':
                label = "Starter"
            else:
                return default(cf)

        if cf_info and cf['id'] in CUSTOM_FIELDS_LABEL_COLOR:
            self.gitlab.label_create_if_needed(label, CUSTOM_FIELDS_LABEL_COLOR[cf['id']])

        if markdown:
            return f'~"{label}"'
        return label

    def format_priority(self, priority, default=str, markdown=True):
        try:
            _priority = "P:"+super().format_priority(priority, default=lambda i: None)
        except TypeError:
            return super().format_priority(priority, default=default)

        if _priority == "P:Normal":
            return "Normal"
        self.gitlab.label_create_if_needed(_priority, '#000FF0')
        if markdown:
            return f'~"{_priority}"'
        return _priority

    def format_category(self, category, default=str, markdown=True):
        try:
            _category = "C:"+super().format_category(category, default=lambda i: None)
        except TypeError:
            return super().format_category(category, default=default)

        self.gitlab.label_create_if_needed(_category, '#8E44AD')
        if markdown:
            return f'~"{_category}"'
        return _category

    def format_version(self, version, default=str):
        _version = super().format_version(version, default=lambda i: None)
        if not _version:
            return super().format_version(version, default=default)

        self.gitlab.milestone_create_if_needed(_version)
        return f'%"{_version}"'

    def format_journal_value_updated(self, what):
        return f"**{what}** updated"

    def format_journal_value_changed(self, what, old_value, new_value):
        if old_value.startswith("~"):
            # Labels can't be combined with stike-through, we desided to prefer to see labels
            return f"**{what}** changed from {old_value} to {new_value}"
        return f"**{what}** changed from ~~{old_value}~~ to {new_value}"

    def format_journal_value_set(self, what, new_value):
        return f"**{what}** set to {new_value}"

    def format_journal_value_added(self, what, new_value):
        return f"**{what}** {new_value} added"

    def format_journal_value_deleted(self, what, old_value):
        if old_value.startswith("~"):
            # Labels can't be combined with stike-through, we desided to prefer to see labels
            return f"**{what}** deleted ({old_value})"
        return f"**{what}** deleted (~~{old_value}~~)"

    def issue_link(self, redmine_issue, note=None, private=False):
        name = str(redmine_issue['id'])
        if private:
            url = f"{self.settings.ARCHIVE_PRIVATE_URL}/issues/{name}"
        else:
            url = f"{self.settings.ARCHIVE_PUBLIC_URL}/issues/{name}"
        if note:
            url += f"#note-{note}"
            name += f"#note-{note}"
        return f"[#{name} (Redmine)]({url})"


class Converter:
    def __init__(self, redmine: Redmine, gitlab: GitlabProject, settings: Settings):
        self.redmine = redmine
        self.gitlab = gitlab
        self.settings = settings
        self.textile_converter = TextileConverter(gitlab, settings)
        self.formatter = GitlabFormatter(redmine, gitlab, settings)
        self.templates = Templates(self.formatter, autoescape=False)
        self.templates.jinja2.filters["textile2markdown"] = self.textile2markdown
        self.templates.jinja2.globals["issue_link"] = self.formatter.issue_link
        self.templates.jinja2.globals["issue_and_no_label"] = self.formatter.issue_and_no_label
        self.template_issue = self.templates.get("issue.md")
        self.template_comment = self.templates.get("comments.md")
        self.set_second_run(False)

    def set_second_run(self, second_run):
        self.second_run = second_run
        self.textile_converter.second_run = second_run

    @jinja2.contextfilter
    def textile2markdown(self, context, text):
        redmine_issue = context["redmine_issue"]
        re_image_link = re.compile(r"(!\[.*?\])\((.*?)\)")

        def repl(m):
            url = self.formatter.resolve_image_url(redmine_issue, m.group(2))
            if url.startswith("http://redmine.tails.boum.org"):
                url = url.replace("http://", "https://", 1)
            return f"{m.group(1)}({url})"
        text = self.textile_converter.convert(text, private=redmine_issue.get("is_private", False))
        text = re_image_link.sub(repl, text)
        return text

    def issue(self, redmine_issue):
        self.formatter.annotate_issue(redmine_issue)

        labels = []
        for label in gitlab_status_labels(redmine_issue):
            self.gitlab.label_create_if_needed(label, "#F0AD4E")
            labels.append(label)

        type_of_work = None
        for custom_field in redmine_issue.get('custom_fields', ()):
            if not custom_field.get("value", None):
                continue
            if custom_field["id"] == 15:
                type_of_work = custom_field["value"]

            name = custom_field["name"]
            if name in CUSTOM_FIELDS_LABEL_COLOR:
                label = self.formatter.format_custom_field(custom_field, default=lambda i: None, markdown=False)
                if label:
                    labels.append(label)

        priority = redmine_issue.get('priority', None)
        if priority and priority['name'] != 'Normal':
            name = self.formatter.format_priority(priority, markdown=False)
            if name:
                labels.append(name)

        category = redmine_issue.get('category', {})
        if category:
            name = self.formatter.format_category(category, default=lambda i: None, markdown=False)
            if name:
                labels.append(name)

        custom_fields = {}
        for cf in redmine_issue.get("custom_fields", ()):
            custom_fields[cf["id"]] = cf

        for rel in self.templates.issue_relations(redmine_issue):
            if rel.get('label', None) == "Blocks" and int(rel['other']['id']) in CORE_WORK:
                label = self.formatter.core_work_label(CORE_WORK[int(rel['other']['id'])])
                labels.append(label)

        description = self.template_issue.render(redmine_issue=redmine_issue, custom_fields=custom_fields)

        issue = {
                'iid': redmine_issue['id'],
                'title': redmine_issue['subject'],
                'project' : self.settings.GITLAB_PROJECT,
                'author': MIGRATION_USER,
                'description': description.strip(),
                'state_event': gitlab_status(redmine_issue),
                'created_at': redmine_issue['created_on'],
                'updated_at': redmine_issue['updated_on'],
                }

        try:
            author = self.formatter.format_user(redmine_issue.get("author",None), default=lambda u: None, markdown=False)
            if author and author != "Tails":
                issue["author"] = author.replace("@","_")
        except AttributeError:
            pass

        try:
            assignee = self.formatter.format_user(redmine_issue.get("assigned_to",None), default=lambda u: None, markdown=False)
            if assignee:
                _a = self.gitlab.user_create_if_needed(assignee, data={})
                issue["assignee_ids"] = [_a['id']]
        except AttributeError:
            pass

        if issue['state_event'] == "close":
            issue['updated_at'] = redmine_issue['closed_on']

        if 'fixed_version' in redmine_issue:
            issue['milestone_id'] = self.gitlab.milestone_create_if_needed(redmine_issue['fixed_version']['name'])['id']

        if redmine_issue.get('is_private', False):
            issue['confidential'] = True
            issue['project'] = f'{self.settings.GITLAB_NAMESPACE}/private'

        if type_of_work == "Accounting":
            issue['project'] = f'{self.settings.GITLAB_NAMESPACE}/accounting'
        elif type_of_work == "Sysadmin":
            issue['project'] = f'{self.settings.GITLAB_NAMESPACE}/sysadmin'
        elif category.get('name',None) == "Fundraising":
            issue['project'] = f'{self.settings.GITLAB_NAMESPACE}/fundraising'

        if issue['project'] != self.settings.GITLAB_PROJECT:
            self.gitlab.project_create_if_needed(issue['project'])

        if labels:
            issue['labels'] = labels

        return issue

    def stub_issue(self, gitlab_issue):
        issue = copy.copy(gitlab_issue)
        issue['project'] = self.settings.GITLAB_PROJECT
        if issue.get('milestone_id', None):
            del(issue['milestone_id'])
        if issue.get('assignee_ids', None):
            del(issue['assignee_ids'])
        if issue.get('updated_at', None):
            del(issue['updated_at'])
        issue['author'] = MIGRATION_USER
        self.gitlab.label_create_if_needed('stub issue', "#F0AD4E")
        issue['labels'] = ['stub issue',]
        issue['title'] = "Stub issue"
        issue['state_event'] = "close"
        issue['description'] = f"This issue was migrated to {gitlab_issue['project']}#{issue['iid']}."
        return issue

    def comments(self, redmine_issue: int):
        comments = []
        self.formatter.annotate_issue(redmine_issue)
        for pos, event in enumerate(redmine_issue['journals']):
            private = False
            if event.get("private_notes", None):
                private = True
            if not self.templates.journal_has_details(event) and not event.get("notes", "").strip():
                private = True
            if private:
                body = f"A confidential comment was added. If you have enough permissions, you can see it on {self.formatter.issue_link(redmine_issue,pos+1, private=True)}."
            else:
                body = self.template_comment.render(event=event, redmine_issue=redmine_issue, pos=pos+1)
            comment = {'body': body.strip(),
                       'author': MIGRATION_USER,
                       'created_at': event['created_on'],
                       'updated_at': event['created_on'],
                      }
            if not private:
                try:
                    author = self.formatter.format_user(event.get("user", None), default=lambda u: None, markdown=False)
                    if author and author != "Tails":
                        comment["author"] = author.replace("@", "_")
                except AttributeError:
                    pass

            comments.append(comment)

        return comments

    def create(self, redmine_issue):
        number = redmine_issue["id"]
        private_issue = redmine_issue.get('is_private', False)
        if private_issue:
            log.info(f"Processing private #{number}")
        else:
            log.info(f"Processing #{number}")

        func = functools.partial(self.issue, redmine_issue)
        self.gitlab.create_issue(number, func)

        func = functools.partial(self.comments, redmine_issue)
        self.gitlab.create_comments(number, len(redmine_issue['journals']), func)
        return number

    def update(self, redmine_issue):
        number = redmine_issue["id"]
        private_issue = redmine_issue.get('is_private', False)
        if private_issue:
            log.info(f"Updating private #{number}")
        else:
            log.info(f"Updating #{number}")
        gitlab_issue = self.issue(redmine_issue)
        comments = self.comments(redmine_issue)
        self.gitlab.update_issue(gitlab_issue)
        if comments:
            self.gitlab.update_comments(number, comments)
        if gitlab_issue['project'] != self.settings.GITLAB_PROJECT:
            log.info(f"Stub issue for #{number}")
            stub_issue = self.stub_issue(gitlab_issue)
            self.gitlab.create_or_update_stub_issue(stub_issue)
            # the second run will close the ticket correctly
            self.gitlab.create_or_update_stub_issue(stub_issue)
        for watcher in redmine_issue.get("watchers", []):
            if isinstance(watcher, (int, str)):
                user_id = int(watcher)
                redmine_user = self.redmine.user_or_group(user_id)
            else:
                redmine_user = self.redmine.user_or_group(watcher['id'])
            if redmine_user is None:
                continue
            gitlab_user = self.gitlab.users.get(redmine_user['login'], None)
            if gitlab_user is None:
                # Don't create users, if they are only "watchers"
                continue
            self.gitlab.subscribe_user(gitlab_issue, gitlab_user)
        return number
