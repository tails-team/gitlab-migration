from .cmdline import Command, Fail
from .redmine import Redmine
from .gitlab import GitlabProject

import datetime
import logging
import yaml

log = logging.getLogger(__name__)

class Gitlabracadabra(Command):
    """
    Creates a configuration file for Gitlabracadabra.
    https://gitlab.com/gitlabracadabra/gitlabracadabra
    """
    @classmethod
    def make_subparser(cls, subparsers):
        parser = super().make_subparser(subparsers)
        parser.add_argument("--progress", action="store_true", help="show progress")
        return parser

    def run(self):
        """
        Upload Redmine data to Gitlab
        """
        redmine = Redmine(self.settings)
        gitlab = GitlabProject(self.settings)

        if self.args.progress:
            from .utils import progress
            show_progress = progress
        else:
            def show_progress(x):
                return x

        config = dict()
        config['milestones'] = []
        milestones = config['milestones']
        proj = 105
        #for proj in redmine.projects.list():
        versions = redmine.projects.versions(proj)
        for version in show_progress(versions):
            if version['name'].lower().startswith("sponsor"):
                continue
            milestone = {'title': version['name'],
                         'description': version['description'],
                         'state': 'active',
                        }

            if version.get('due_date', None):
                milestone['due_date'] = version['due_date']

            if version['status'] == "closed":
                milestone['state'] = 'closed'
                if version.get('due_date', None):
                    due_date = datetime.datetime.strptime(version['due_date'],'%Y-%m-%d').date()
                    milestone['start_date'] = (due_date - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
            milestones.append(milestone)

        with open(self.settings.GITLAB_CONFIG+"/milestones.yml", 'w') as f:
            yaml.safe_dump(config, f, default_flow_style=False)
