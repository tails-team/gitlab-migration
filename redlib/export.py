from .cmdline import Command, Fail
from .redmine import Redmine
import logging
import os
import sys
import json
import yaml

log = logging.getLogger(__name__)


def yaml_dump(data, stream=None):
    return yaml.dump(
            data, stream=stream, default_flow_style=False,
            allow_unicode=True, explicit_start=True, Dumper=yaml.CDumper)


class Export(Command):
    """
    Various data export functions from redmine
    """

    @classmethod
    def make_subparser(cls, subparsers):
        parser = super().make_subparser(subparsers)
        parser.add_argument("--attachment-urls", action="store_true",
                            help="output a mapping of redmine attachment urls to archive attachment urls")
        parser.add_argument("--user-emails", action="store_true",
                            help="export user email information from redmine into a gitlabracadabra dataset")
        return parser

    def export_attachment_urls(self, redmine):
        res = []
        for number in redmine.all_our_issue_ids():
            issue = redmine.issues.issue(number)
            for a in issue.get("attachments", ()):
                src_fname = redmine.attachment(a["id"], a["content_url"])
                if src_fname is None:
                    log.warn("Cannot get filename for attachment %r", a)
                    continue
                dest_url = f"attachments/download/{a['id']}/{os.path.basename(src_fname)}"
                if issue.get("is_private"):
                    dest_url = f"{self.settings.ARCHIVE_PRIVATE_URL}/{dest_url}"
                else:
                    dest_url = f"{self.settings.ARCHIVE_PUBLIC_URL}/{dest_url}"
                res.append({
                    "redmine": a["content_url"],
                    "archive": dest_url,
                    "private": bool(issue.get("is_private")),
                })
        json.dump(res, sys.stdout, indent=1)
        sys.stdout.write("\n")

    def export_user_emails(self, redmine):
        res = {}
        for user_id in redmine.active_user_ids():
            user = redmine.user(user_id)
            if user is None:
                log.warn("No information found for user %d", user_id)
                continue
            email = user.get("mail")
            if email is None:
                log.warn("User without email: %r", user)
                continue
            res[user["login"]] = {
                "type": "user",
                "email": user["mail"],
            }
        yaml_dump(res, stream=sys.stdout)

    def run(self):
        redmine = Redmine(self.settings)

        if self.args.attachment_urls:
            self.export_attachment_urls(redmine)
        if self.args.user_emails:
            self.export_user_emails(redmine)
        else:
            raise Fail("please use a command line switch to select what to do. See --help")
