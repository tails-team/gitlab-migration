# Based on https://github.com/redmine-gitlab-migrator/redmine-gitlab-migrator
# License: GPLv3
from __future__ import annotations
from typing import Any, Dict, List, Optional, Sequence
import os
import logging

from .utils import APIClient, Workdir, atomic_writer
from .cmdline import Fail
from .settings import Settings
import requests

# ANONYMOUS_USER_ID = 2

log = logging.getLogger(__name__)


# Based on https://github.com/redmine-gitlab-migrator/redmine-gitlab-migrator
# License: GPLv3
class RedmineClient(APIClient):
    PAGE_MAX_SIZE = 100

    def __init__(self, api_key):
        super().__init__()
        self.session.headers["X-Redmine-API-Key"] = api_key

    def get(self, *args, **kwargs):
        # In detail views, redmine encapsulate "foo" typed objects under a
        # "foo" key on the JSON.
        ret = super().get(*args, **kwargs)
        values = ret.values()
        if len(values) == 1:
            return list(values)[0]
        else:
            return ret

    def get_optional(self, url, *args, **kw):
        try:
            return self.get(url, *args, **kw)
        except requests.exceptions.HTTPError as e:
            if e.response.status_code != 404:
                raise
            else:
                log.warn("%s: not found on server (404): skipping", url)
                return None

    def unpaginated_get(self, *args, **kwargs):
        """
        Iterates over API pagination for a given resource list
        """
        kwargs['params'] = kwargs.get('params', {})
        kwargs['params']['limit'] = self.PAGE_MAX_SIZE

        log.info("Get %s [first batch]", args[0])
        resp = self.get(*args, **kwargs)

        # Try to autofind the top-level key containing
        keys_candidates = (
            set(resp.keys()) - set(['total_count', 'offset', 'limit']))

        assert len(keys_candidates) == 1
        res_list_key = list(keys_candidates)[0]

        yield from resp[res_list_key]
        if 'offset' not in resp:
            return

        while (resp['total_count'] - resp['offset'] - resp['limit']) > 0:
            offset = (kwargs['params'].get('offset', 0) + self.PAGE_MAX_SIZE)
            kwargs['params']['offset'] = offset
            log.info("Get %s [offset #%d]", args[0], offset)
            resp = self.get(*args, **kwargs)
            yield from resp[res_list_key]

    def download_attachment(self, url: str, dest: str):
        """
        Download a file attachment from a url to the given pathname
        """
        res = self.session.get(url, stream=True)
        try:
            res.raise_for_status()
        except requests.exceptions.HTTPError as e:
            if e.response.status_code != 404:
                raise
            else:
                log.warn("%s: attachment not found on server (404): skipping", url)
                return

        with atomic_writer(dest, "wb") as fd:
            for chunk in res.iter_content(chunk_size=128):
                fd.write(chunk)


class Projects:
    def __init__(self, redmine: "Redmine", storage_root: str):
        self.redmine = redmine
        self.storage = Workdir(storage_root)
        self._versions_warned = set()
        self._categories_warned = set()

    def list(self) -> List[Dict[Any, Any]]:
        """
        Return a list with all the project IDs

        See https://www.redmine.org/projects/redmine/wiki/Rest_Projects
        """
        key = "list"
        if not self.storage.has(key):
            log.info("Loading project list")
            projects = self.redmine.api.unpaginated_get(f'{self.redmine.instance_url}/projects.json')
            self.storage.set(key, list(projects))
        return self.storage.get(key)

    def project(self, number: int) -> Optional[Dict[Any, Any]]:
        """
        Return the details of the given project

        See https://www.redmine.org/projects/redmine/wiki/Rest_Projects
        """
        key = str(number)
        if not self.storage.has(key):
            log.info("Loading project %d", number)
            url = f"{self.redmine.instance_url}/projects/{number}.json"
            project = self.redmine.api.get_optional(url)
            self.storage.set(key, project)
        return self.storage.get(key)

    def membership(self, number: int) -> Dict[Any, Any]:
        """
        Return the membership information for the given project

        See https://www.redmine.org/projects/redmine/wiki/Rest_Memberships
        """
        key = f"{number}-membership"
        if not self.storage.has(key):
            log.info("Loading project %d membership", number)
            url = f"{self.redmine.instance_url}/projects/{number}/memberships.json"
            membership = self.redmine.api.get(url)
            self.storage.set(key, membership)
        return self.storage.get(key, None)

    def versions(self, number: int) -> Dict[Any, Any]:
        """
        Return the version information for the given project

        See https://www.redmine.org/projects/redmine/wiki/Rest_Versions
        """
        key = f"{number}-versions"
        if not self.storage.has(key):
            log.info("Loading project %d versions", number)
            url = f"{self.redmine.instance_url}/projects/{number}/versions.json"
            try:
                versions = list(self.redmine.api.unpaginated_get(url))
            except requests.exceptions.HTTPError as e:
                log.error("Project %s: cannot download version list: %s", number, e)
                versions = []
            self.storage.set(key, versions)
        return self.storage.get(key, None)

    def version(self, project_id: int, version_id: int) -> Optional[Dict[Any, Any]]:
        """
        Return the record for the given version
        """
        for v in self.versions(project_id):
            if v["id"] == int(version_id):
                return v
        if version_id not in self._versions_warned:
            log.warn("Version %d not found on project %d", version_id, project_id)
            self._versions_warned.add(version_id)
        return None

    def issue_categories(self, number: int) -> List[Dict[Any, Any]]:
        """
        Return the list of issue categories for the given project

        See https://www.redmine.org/projects/redmine/wiki/Rest_IssueCategories
        """
        key = f"{number}-issue-categories"
        if not self.storage.has(key):
            log.info("Loading project %d issue categories", number)
            url = f"{self.redmine.instance_url}/projects/{number}/issue_categories.json"
            try:
                issue_categories = list(self.redmine.api.unpaginated_get(url))
            except requests.exceptions.HTTPError as e:
                log.error("Project %s: cannot download category list: %s", number, e)
                issue_categories = []
            self.storage.set(key, issue_categories)
        return self.storage.get(key, None)

    def issue_category(self, project_id: int, category_id: int) -> Optional[Dict[Any, Any]]:
        """
        Return the record for the given issue category
        """
        for cat in self.issue_categories(project_id):
            if cat["id"] == int(category_id):
                return cat
        if category_id not in self._categories_warned:
            log.warn("Category %d not found on project %d", category_id, project_id)
            self._categories_warned.add(category_id)
        return None


class Issues:
    def __init__(self, redmine: "Redmine", storage_root: str):
        self.redmine = redmine
        self.storage = Workdir(storage_root)
        self._issues_warned = set()

    def list(self, project_id) -> List[Dict[Any, Any]]:
        """
        Return a list with all the issue IDs

        See https://www.redmine.org/projects/redmine/wiki/Rest_Issues
        """
        key = f"list-{project_id}"
        if not self.storage.has(key):
            log.info("Loading issue list")
            url = f'{self.redmine.instance_url}/issues.json?status_id=*&project_id={project_id}'
            issues = self.redmine.api.unpaginated_get(url)
            try:
                self.storage.set(key, list(issues))
            except requests.exceptions.HTTPError as e:
                log.error("Project %s: cannot download issue list: %s", project_id, e)
                self.storage.set(key, [])
        return self.storage.get(key)

    def statuses(self) -> List[Dict[Any, Any]]:
        """
        Return a list with all the issue statuses

        See https://www.redmine.org/projects/redmine/wiki/Rest_IssueStatuses
        """
        key = "statuses"
        if not self.storage.has(key):
            log.info("Loading issue statuses")
            statuses = self.redmine.api.get(f'{self.redmine.instance_url}/issue_statuses.json')
            self.storage.set(key, list(statuses))
        return self.storage.get(key)

    def status(self, number: int) -> Optional[Dict[Any, Any]]:
        """
        Return information for the issue status with the given number
        """
        for status in self.statuses():
            if status["id"] == number:
                return status

        extra_statuses = self.redmine.settings.REDMINE_EXTRA_INFO.get("status", {})
        status = extra_statuses.get(number)
        if status is not None:
            log.debug("Status %d resolved from REDMINE_EXTRA_INFO", number)
            return status
        log.warn("Status %d not found", number)
        return None

    def issue(self, number: int) -> Optional[Dict[Any, Any]]:
        """
        Return the details of the given issue

        See https://www.redmine.org/projects/redmine/wiki/Rest_Issues
        """
        if number not in self.redmine.all_our_issue_ids():
            if number not in self._issues_warned:
                log.info("#%d: skipping apparently mistyped issue", number)
                self._issues_warned.add(number)
            return None

        key = str(number)
        if not self.storage.has(key):
            log.info("Loading issue %d", number)
            issue_url = (f"{self.redmine.instance_url}/issues/{number}.json?"
                         "include=journals,watchers,relations,children,attachments,changesets")
            issue = self.redmine.api.get(issue_url)
            #     # if e.response.status_code != 500:
            #     #     raise
            #     # Prevent silently ignoring tickets if redmine is having
            #     # transient server errors
            #     # if number in self.redmine.all_our_issue_ids():
            #     #     raise
            #     log.warn("#%d: skipping apparently mistyped issue", number)
            #     issue = None
            self.storage.set(key, issue)
        return self.storage.get(str(number))

    def relations(self, number: int) -> Dict[Any, Any]:
        """
        Return the relations of the given issue

        See https://www.redmine.org/projects/redmine/wiki/Rest_IssueRelations
        """
        key = f"{number}-relations"
        if not self.storage.has(key):
            log.info("Loading relations of issue %d", number)
            issue_url = f"{self.redmine.instance_url}/issues/{number}/relations.json"
            relations = self.redmine.api.get(issue_url)
            self.storage.set(key, relations)
        return self.storage.get(key)


class Redmine:
    def __init__(self, settings: Settings):
        self.settings = settings
        if self.settings.REDMINE_URL is None:
            raise Fail("REDMINE_URL is not set in the configuration")
        self.instance_url = self.settings.REDMINE_URL.rstrip("/")

        self._api = None

        root = os.path.join(settings.WORKDIR, "redmine")
        self.projects = Projects(self, os.path.join(root, "projects"))
        self.issues = Issues(self, os.path.join(root, "issues"))
        self.users = Workdir(os.path.join(root, "users"))
        self.groups = Workdir(os.path.join(root, "groups"))
        self.queries = Workdir(os.path.join(root, "queries"))
        self.trackers = Workdir(os.path.join(root, "trackers"))
        self.enums = Workdir(os.path.join(root, "enums"))
        self.roles = Workdir(os.path.join(root, "roles"))
        self.custom_fields = Workdir(os.path.join(root, "custom_fields"))

        self.attachments_root = os.path.join(root, "attachments")
        os.makedirs(self.attachments_root, exist_ok=True)

        self._all_our_issue_ids = None
        self._categories_warned = set()
        self._versions_warned = set()

    @property
    def api(self):
        if self.settings.REDMINE_API_KEY is None:
            raise Fail("REDMINE_API_KEY is not set in the configuration")
        self._api = RedmineClient(self.settings.REDMINE_API_KEY)
        return self._api

    def project_ids(self) -> List[int]:
        """
        Return a list of all relevant project IDs
        """
        if self.settings.REDMINE_PROJECTS_INCLUDE:
            project_ids = []
            for proj in self.projects.list():
                if proj["name"] in self.settings.REDMINE_PROJECTS_INCLUDE:
                    project_ids.append(proj["id"])
            project_ids.sort()
            return project_ids
        else:
            return sorted(i["id"] for i in self.projects.list())

    def all_issues(self):
        """
        Return a merged list of all issues
        """
        res = []
        for project_id in self.project_ids():
            res.extend(self.issues.list(project_id))
        return res

    def user_list(self) -> List[Dict[Any, Any]]:
        """
        Return a list with all the user IDs

        See https://www.redmine.org/projects/redmine/wiki/Rest_Users
        """
        users = self.users.get("list", None)
        if users is None:
            log.info("Loading user list")
            users = self.api.unpaginated_get(f'{self.instance_url}/users.json')
            users = list(users)
            self.users.set("list", users)
        return users

    def active_user_ids(self):
        """
        Return a sequence with the IDs of users who have had any activity in
        redmine
        """
        ids = set()

        def maybe_add(rec):
            if rec is None:
                return
            user_id = rec.get("id")
            if user_id is None:
                return
            ids.add(user_id)

        for number in self.all_our_issue_ids():
            issue = self.issues.issue(number)
            maybe_add(issue.get("author"))
            maybe_add(issue.get("assigned_to"))
            for j in issue.get("journals", ()):
                maybe_add(j.get("user"))
            for w in issue.get("watchers", ()):
                maybe_add(w)

        return ids

    def issues_related_to(self, issue_id: int) -> Sequence[int]:
        """
        Return a sequence of IDs of issues related to the given one
        """
        res = set()
        issue = self.issues.issue(issue_id)
        for rel in issue.get("relations", ()):
            if rel.get("relation_type") != "relates":
                continue
            res.add(rel["issue_id"])
            res.add(rel["issue_to_id"])
        res.remove(issue_id)
        return res

    def group_list(self) -> List[Dict[Any, Any]]:
        """
        Return a list with all the group IDs

        See https://www.redmine.org/projects/redmine/wiki/Rest_Groups
        """
        groups = self.groups.get("list", None)
        if groups is None:
            log.info("Loading group list")
            groups = self.api.get(f'{self.instance_url}/groups.json')
            groups = list(groups)
            self.groups.set("list", groups)
        return groups

    def user(self, number: int) -> Dict[Any, Any]:
        """
        Return the details of the given user

        See https://www.redmine.org/projects/redmine/wiki/Rest_Users
        """
        if not self.users.has(str(number)):
            log.info("Loading user %d", number)
            user_url = f"{self.instance_url}/users/{number}.json"
            user = self.api.get_optional(user_url)
            self.users.set(str(number), user)
        return self.users.get(str(number), None)

    def user_or_group(self, number: int) -> Dict[Any, Any]:
        """
        Return the details of the given user or group
        """
        u = self.user(number)
        if u is not None:
            return u

        for g in self.group_list():
            if g["id"] == number:
                return g

        log.warn("User or group %d not found", number)
        return None

    def query_list(self) -> List[Dict[Any, Any]]:
        """
        Return a list with all the custom queries

        See https://www.redmine.org/projects/redmine/wiki/Rest_Queries
        """
        queries = self.queries.get("list", None)
        if queries is None:
            log.info("Loading query list")
            queries = self.api.unpaginated_get(f'{self.instance_url}/queries.json')
            queries = list(queries)
            self.queries.set("list", queries)
        return queries

    def tracker_list(self) -> List[Dict[Any, Any]]:
        """
        Return a list with all the custom trackers

        See https://www.redmine.org/projects/redmine/wiki/Rest_Trackers
        """
        trackers = self.trackers.get("list", None)
        if trackers is None:
            log.info("Loading tracker list")
            trackers = self.api.get(f'{self.instance_url}/trackers.json')
            trackers = list(trackers)
            self.trackers.set("list", trackers)
        return trackers

    def tracker(self, number: int) -> Optional[Dict[Any, Any]]:
        """
        Return information for the tracker with the given number
        """
        for tracker in self.tracker_list():
            if tracker["id"] == number:
                return tracker
        log.warn("Tracker %d not found", number)
        return None

    def issue_priority_list(self) -> List[Dict[Any, Any]]:
        """
        Return a list with all the issue priorities

        See https://www.redmine.org/projects/redmine/wiki/Rest_Enumerations
        """
        priorities = self.enums.get("priorities", None)
        if priorities is None:
            log.info("Loading issue priority list")
            priorities = self.api.get(f'{self.instance_url}/enumerations/issue_priorities.json')
            priorities = list(priorities)
            self.enums.set("priorities", priorities)
        return priorities

    def priority(self, number: int) -> Optional[Dict[Any, Any]]:
        """
        Return information for the issue priority with the given number
        """
        for prio in self.issue_priority_list():
            if prio["id"] == number:
                return prio
        log.warn("Issue priority %d not found", number)
        return None

    def document_category_list(self) -> List[Dict[Any, Any]]:
        """
        Return a list with all the issue priorities

        See https://www.redmine.org/projects/redmine/wiki/Rest_Enumerations
        """
        categories = self.enums.get("document_categories", None)
        if categories is None:
            log.info("Loading document category list")
            categories = self.api.get(f'{self.instance_url}/enumerations/document_categories.json')
            categories = list(categories)
            self.enums.set("document_categories", categories)
        return categories

    def roles_list(self) -> List[Dict[Any, Any]]:
        """
        Return a list with all the role IDs

        See https://www.redmine.org/projects/redmine/wiki/Rest_Roles
        """
        roles = self.roles.get("list", None)
        if roles is None:
            log.info("Loading roles list")
            roles = self.api.get(f'{self.instance_url}/roles.json')
            roles = list(roles)
            self.roles.set("list", roles)
        return roles

    def role(self, number: int) -> Dict[Any, Any]:
        """
        Return the details of the given role

        See https://www.redmine.org/projects/redmine/wiki/Rest_Roles
        """
        role = self.roles.get(str(number), None)
        if role is None:
            log.info("Loading role %d", number)
            role = self.api.get(f"{self.instance_url}/roles/{number}.json?")
            self.roles.set(str(number), role)
        return role

    def custom_field_list(self) -> List[Dict[Any, Any]]:
        """
        Return a list with all the custom fields

        See https://www.redmine.org/projects/redmine/wiki/Rest_CustomFields
        """
        fields = self.custom_fields.get("list", None)
        if fields is None:
            log.info("Loading custom fields list")
            fields = self.api.get(f'{self.instance_url}/custom_fields.json')
            fields = list(fields)
            self.custom_fields.set("list", fields)
        return fields

    def custom_field(self, number: int) -> Dict[Any, Any]:
        """
        Return information for the custom field with the given number
        """
        for f in self.custom_field_list():
            if f["id"] == number:
                return f
        raise KeyError(f"custom field {number} not found")

    def attachment(self, id: int, url: str) -> str:
        """
        Download the given attachment ID from the given URL and return a local
        pathname to it

        See https://www.redmine.org/projects/redmine/wiki/Rest_Attachments
        """
        from urllib.parse import urlparse, unquote_plus
        parsed = urlparse(url)
        filename = unquote_plus(os.path.basename(parsed.path))
        if "/" in filename:
            raise RuntimeError("Got insecure filename {}".format(filename))

        destdir = os.path.join(self.attachments_root, str(id))
        os.makedirs(destdir, exist_ok=True)

        missing_flag = os.path.join(destdir, filename + ".missing")
        if os.path.exists(missing_flag):
            return None

        pathname = os.path.join(destdir, filename)
        if not os.path.exists(pathname):
            log.info("Dowloading attachment #%d: %s", id, filename)
            self.api.download_attachment(url, pathname)

        if not os.path.exists(pathname):
            log.info("Attachment #%d: %s not found on server", id, filename)
            # Attachment gave 404 on the server
            # Mark it as missing to avoid retrying each time
            with open(missing_flag, "wb"):
                pass
            return None

        return pathname

    def category(self, category_id: int) -> Optional[Dict[Any, Any]]:
        """
        Look up the given category id in all projects
        """
        found = []
        for project_id in self.project_ids():
            category = self.projects.issue_category(project_id, category_id)
            if category is not None:
                found.append(category)
        if len(found) > 1:
            if category_id not in self._categories_warned:
                log.warn("Found multiple records for category_id=%d: %r", category_id, found)
                self._categories_warned.add(category_id)
            return None
        if not found:
            if category_id not in self._categories_warned:
                log.warn("Found no information for category_id=%d", category_id)
                self._categories_warned.add(category_id)
            return None
        return found[0]

    def version(self, version_id: int) -> Optional[Dict[Any, Any]]:
        """
        Look up the given version id in all projects
        """
        found = []
        for project_id in self.project_ids():
            version = self.projects.version(project_id, version_id)
            if version is not None:
                found.append(version)
        if len(found) > 1:
            if version_id not in self._versions_warned:
                log.warn("Found multiple records for version_id=%d: %r", version_id, found)
                self._versions_warned.add(version_id)
            return None
        if not found:
            if version_id not in self._versions_warned:
                log.warn("Found no information for version_id=%d", version_id)
                self._versions_warned.add(version_id)
            return None
        return found[0]

    def all_our_issue_ids(self):
        """
        Return a set with all our issue ids
        """
        if self._all_our_issue_ids is None:
            all_issues = set()
            for project_id in self.project_ids():
                for issue in self.issues.list(project_id):
                    all_issues.add(issue["id"])
            self._all_our_issue_ids = all_issues
        return self._all_our_issue_ids

#     def get_participants(self):
#         """Get participating users (issues authors/owners)
#
#         :return: list of all users participating on issues
#         :rtype: list
#         """
#         user_ids = set()
#         users = []
#
#         for i in self.get_all_issues():
#             journals = i.get('journals', [])
#             for i in chain(i.get('watchers', []),
#                            [i['author'], i.get('assigned_to', None)]):
#
#                 if i is None:
#                     continue
#                 user_ids.add(i['id'])
#             for entry in journals:
#                 if not entry.get('notes', None):
#                     continue
#                 user_ids.add(entry['user']['id'])
#
#         for i in user_ids:
#             # The anonymous user is not really part of the project...
#             # You may want to add Group IDs such as [ANONYMOUS_USER_ID, 324, 234, ...] if necessary
#             if i not in [ANONYMOUS_USER_ID]:
#                 try:
#                     users.append(self.api.get('{}/users/{}.json'.format(
#                         self.instance_url, i)))
#                 except HTTPError:
#                     print("unable to retrieve user!")
#
#         return users
