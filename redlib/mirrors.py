from .cmdline import Command
from .gitlab import GitLabClient, GitlabProject

import logging


log = logging.getLogger(__name__)


class Mirrors(Command):
    """
    Create remote mirrors in the Tails infrastructure
    """

    def _api(self):
        url = self.settings.GITLAB_URL
        key = self.settings.GITLAB_API_KEY
        api = GitLabClient(url, key)
        return api

    def _is_mirrored(self, api, project, url):
        mirrors = api.project_get('remote_mirrors', project=project)
        log.info(f"Mirrors: {mirrors}")
        urls = map(lambda m: m['url'], mirrors)
        log.info(f"Mirror urls: {urls}")
        if filter(lambda u: u == url, urls):
            return True

    def run(self):
        api = self._api()
        json = {'enabled': True, 'only_protected_branches': False}
        for project_name in self.settings.MIRRORS:
            log.info(f'Looking at "{project_name}"')
            gitlab = GitlabProject(self.settings)
            project = gitlab.project('tails/' + project_name)
            url = f'{self.settings.GITOLITE_URL}/{project_name}'
            if self._is_mirrored(api, project, url):
                log.info(f'Remote mirror for "{project_name}" is already configured')
                continue
            json['url'] = url
            log.info(f'Configuring remote mirror for "{project_name}"')
            ret = api.project_post('remote_mirrors', project=project, json=json)
            if ret.status_code != 201:
                raise Exception(ret.text)
