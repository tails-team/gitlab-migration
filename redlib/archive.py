from .cmdline import Command
from .redmine import Redmine
from .redmine_format import Formatter
from .templates import Templates
from .utils import atomic_writer
from .textileconverter import preescape_urls
from lxml import etree
import jinja2
import pypandoc
import os
import shutil
import datetime
import re
import io
import logging

log = logging.getLogger(__name__)


# def html_i(val):
#     return jinja2.Markup("<i>{}</i>").format(jinja2.escape(val))


class HTMLFormatter(Formatter):
    def __init__(self, site, private=False):
        super().__init__(site.redmine, private)
        self.site = site

#    def format_date(self, date):
#        return html_i(super().format_date(date))

#    def format_project(self, project):
#        return html_i(super().format_project(project))

#    def format_status(self, status):
#        return html_i(super().format_status(status))

#    def format_tracker(self, tracker):
#        return html_i(super().format_tracker(tracker))

#    def format_user(self, user):
#        return html_i(super().format_user(user))

#    def format_priority(self, priority):
#        return html_i(super().format_priority(priority))

#    def format_category(self, category):
#        return html_i(super().format_category(category))

#    def format_version(self, version):
#        return html_i(super().format_version(version))

    def issue_classes(self, issue):
        if issue is None or isinstance(issue, (str, int)):
            return ""
        classes = [
            f"tracker-{issue['tracker']['id']}",
            f"status-{issue['status']['id']}",
            f"priority-{issue['priority']['id']}",
            "priority-default",
        ]
        if "closed_on" in issue:
            classes.append("closed")
        return " ".join(classes)

    def journal_classes(self, entry):
        classes = ["journal"]
        if entry.get("details", ()):
            classes.append("has-details")
        if entry.get("private_notes"):
            classes.append("private-notes")
        return " ".join(classes)

    def issue_url(self, issue):
        return f"{self.site.url_prefix}issues/{issue['id']}"

    def format_issue(self, issue, with_subject=False, note=None):
        if isinstance(issue, (int, str)):
            issue_id = int(issue)
            issue = self.redmine.issues.issue(issue_id)
            if issue is None:
                return f"#{issue_id}"

        if not self.private and issue.get("is_private"):
            return f"#{issue['id']}"

        fmt = "<a class='issue {classes} child' href='{url}' title='{subject}'>{tracker} #{id}{note}</a>"
        if "closed_on" in issue:
            fmt = "<del>" + fmt + "</del>"
        if with_subject:
            fmt += ": {subject}"

        url = self.issue_url(issue)
        if note:
            url += f"#note-{note}"

        return jinja2.Markup(
            fmt.format(
                classes=self.issue_classes(issue),
                url=jinja2.escape(url),
                subject=jinja2.escape(issue["subject"]),
                tracker=jinja2.escape(issue["tracker"]["name"]),
                id=issue["id"],
                note="" if not note else f"#note-{note}",
            )
        )

#    def format_missing_issue(self, issue_id):
#        return html_i(super().format_missing_issue(issue_id))

    def format_attachment(self, attachment):
        # Copy attachment to output site
        id = attachment["id"]
        src_fname = self.redmine.attachment(id, attachment["content_url"])
        if src_fname is None:
            return "<del>missing: {fname}</del>".format(fname=jinja2.escape(attachment["filename"]))
        destdir = os.path.join(self.site.output_root, "attachments", "download", str(id))
        os.makedirs(destdir, exist_ok=True)
        shutil.copyfile(src_fname, os.path.join(destdir, attachment["filename"]))

        dest_url = self.site.url_prefix + f"attachments/download/{id}/{os.path.basename(src_fname)}"
        return jinja2.Markup("<a href='{url}'>{fname}</a>".format(
            url=jinja2.escape(dest_url),
            fname=jinja2.escape(attachment["filename"])))

    def format_journal_value_updated(self, what):
        return jinja2.Markup("<strong>{what}</strong> updated").format(what=what)

    def format_journal_value_changed(self, what, old_value, new_value):
        return jinja2.Markup("<strong>{what}</strong> changed from <i>{old_value}</i> to <i>{new_value}</i>").format(
            what=what, old_value=old_value, new_value=new_value,
        )

    def format_journal_value_set(self, what, new_value):
        return jinja2.Markup("<strong>{what}</strong> set to <i>{new_value}</i>").format(
            what=what, new_value=new_value)

    def format_journal_value_added(self, what, new_value):
        return jinja2.Markup("<strong>{what}</strong> <i>{new_value}</i> added").format(
            what=what, new_value=new_value)

    def format_journal_value_deleted(self, what, old_value):
        return jinja2.Markup("<strong>{what}</strong> deleted (<del><i>{old_value}</i></del>)").format(
            what=what, old_value=old_value)


class Site:
    re_issue = re.compile(r"#(?P<issue>\d+)(?:#note-(?P<note>\d+))?")

    def __init__(self, redmine, destdir, private=False, url_prefix="/"):
        self.redmine = redmine
        self.settings = redmine.settings
        self.private = private
        if not url_prefix.endswith("/"):
            url_prefix += "/"
        self.url_prefix = url_prefix
        self.formatter = HTMLFormatter(self, private)
        self.site_root = "."
        self.templates = Templates(self.formatter, self.site_root)
        self.templates.jinja2.filters["textile"] = self.textile_render
        self.templates.jinja2.filters["datetime"] = self.format_datetime
        self.templates.jinja2.globals["issue_classes"] = self.formatter.issue_classes
        self.templates.jinja2.globals["journal_classes"] = self.formatter.journal_classes
        self.templates.jinja2.globals["issue_url"] = self.formatter.issue_url
        self.templates.jinja2.globals["static_url"] = self.static_url
        self.template_issue = self.templates.get("issue.html")
        self.output_root = destdir
        self.attachment_root = os.path.join(self.output_root, "attachments")
        self.html_parser = etree.HTMLParser()

    def static_url(self, path):
        return self.url_prefix + "static/" + path

    def format_datetime(self, dt):
        if isinstance(dt, str):
            try:
                dt = datetime.datetime.strptime(dt, "%Y-%m-%dT%H:%M:%SZ")
            except Exception:
                return dt

        text = dt.strftime("%Y-%m-%d %H:%M:%S %Z")
        return jinja2.Markup(f"<a title='{text}' data-ts='{int(dt.timestamp())}'>{text}</a>")

    re_attachment_url = re.compile(
            r"https?://(?:labs.riseup.net|redmine.tails.boum.org)/code/attachments/(?:download/)")

    @jinja2.contextfilter
    def textile_render(self, context, text):
        issue = context["issue"]
        text = preescape_urls(text)
        text = self.re_issue.sub(
                lambda mo: self.formatter.format_issue(int(mo.group("issue")), note=mo.group("note")), text)
        try:
            html = pypandoc.convert_text(text, 'html', format='textile')
            # html = textile.textile(text)
        except Exception:
            log.exception("Cannot render %r with textile", text)
            html = "<pre>{}</pre>".format(jinja2.escape(text))

        # Reparse and fix <img> urls
        changed = False
        tree = etree.parse(io.StringIO(html), self.html_parser)
        root = tree.getroot()
        if root:
            for node in root.iter("img"):
                src = node.attrib.get("src")
                if src is None:
                    continue
                resolved = self.formatter.resolve_image_url(issue, src)
                if resolved != src:
                    node.attrib["src"] = resolved
                    changed = True

        if changed:
            html = etree.tostring(root, method="html", encoding="unicode")

        html = self.re_attachment_url.sub(self.url_prefix + "attachments/download/", html)

        return jinja2.Markup(html)

    def reset(self, clear=True):
        """
        Clear the destination directory and reinitialize it from empty
        """
        if clear and os.path.exists(self.output_root):
            shutil.rmtree(self.output_root)
        os.makedirs(self.output_root, exist_ok=True)

        dest_static_dir = os.path.join(self.output_root, "static")
        if os.path.isdir(dest_static_dir):
            shutil.rmtree(dest_static_dir)
        shutil.copytree(
                os.path.join(self.site_root, "static"),
                os.path.join(self.output_root, "static"))

        os.makedirs(self.attachment_root, exist_ok=True)

    def add_issue(self, issue):
        outdir = os.path.join(self.output_root, "issues", str(issue["id"]))
        outfile = os.path.join(outdir, "index.html")
        if os.path.exists(outfile):
            return

        # Skip private issues
        if issue.get("is_private") and not self.private:
            return

        log.info("#%d: building issue archive page", issue["id"])
        self.formatter.annotate_issue(issue)
        os.makedirs(outdir, exist_ok=True)
        with atomic_writer(os.path.join(outdir, f"index.html"), "wt", sync=False) as out:
            custom_fields = {}
            for cf in issue.get("custom_fields", ()):
                custom_fields[cf["id"]] = cf
            out.write(self.template_issue.render(issue=issue, custom_fields=custom_fields))


class Archive(Command):
    """
    Generate a static browseable mirror of the redmine contents
    """
    @classmethod
    def make_subparser(cls, subparsers):
        parser = super().make_subparser(subparsers)
        parser.add_argument("--output", "-o", metavar="dir", default="archive",
                            help="output directory (default: 'archive')")
        parser.add_argument("--incremental", action="store_true", help="do not clear existing destination contents")
        parser.add_argument("--progress", action="store_true", help="show progress")
        parser.add_argument("--private", action="store_true", help="include private content")
        # FIXME: drop this, and take it from settings.ARCHIVE_PUBLIC_URL / settings.ARCHIVE_PRIVATE_URL
        parser.add_argument("--url-prefix", action="store", default="/",
                            help="URL prefix if deploying not at site root (default: /)")
        parser.add_argument("--issues", action="store", nargs="+", type=int, help="limit to these issue numbers")
        return parser

    def run(self):
        """
        Visit all redmine things to prefetch them locally
        """
        redmine = Redmine(self.settings)

        site = Site(redmine, self.args.output, private=self.args.private, url_prefix=self.args.url_prefix)
        site.reset(clear=not self.args.incremental)

        if self.args.progress:
            from .utils import progress
            show_progress = progress
        else:
            def show_progress(x):
                return x

        issues = self.args.issues
        if not issues:
            issues = sorted(redmine.all_our_issue_ids(), reverse=True)
        for number in show_progress(issues):
            issue = redmine.issues.issue(number)
            if issue is None:
                log.warn("#%d: cannot get issue information: skipping", number)
                continue
            try:
                site.add_issue(issue)
            except Exception:
                log.exception("#%d: generation failed", number)
