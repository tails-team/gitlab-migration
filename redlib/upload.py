from __future__ import annotations
from typing import Iterable
from .cmdline import Command
from .redmine import Redmine
from .gitlab import GitlabProject
from .converter import Converter
import logging
import os

log = logging.getLogger(__name__)


class ResumeFile:
    def __init__(self, path):
        self.path = path
        self.last = 999999999
        if os.path.exists(path):
            with open(self.path, "rt") as fd:
                last = fd.read().strip()
                if last:
                    self.last = int(last)

    def set_last(self, number: int):
        self.last = number
        with open(self.path, "wt") as fd:
            fd.write(str(self.last))


class Upload(Command):
    """
    Upload the stored json files to a Gitlab instance.
    """
    @classmethod
    def make_subparser(cls, subparsers):
        parser = super().make_subparser(subparsers)
        parser.add_argument("--progress", action="store_true", help="show progress")
        parser.add_argument("--issues", action="store", nargs="+", type=int, help="limit to these issue numbers")
        parser.add_argument("--resume", action="store",
                            help="store the last processed number in this file,"
                                 " and resume from there in subsequent runs")
        parser.add_argument("--first_run", action="store_true", help="Create tickets and comments")
        parser.add_argument("--second_run", action="store_true", help="Run update on tickets")
        parser.add_argument("--parallel", "-n", action="store",
                            help="'n/m', representing this instance n of a pool of m parallel invocations")
        parser.add_argument("--list-issues", action="store_true", help="List issues instead of processing them")
        parser.add_argument("--changed-issues", action="store", metavar="ref1[..ref2]",
                            help="process issue numbers that changed in the git-saved cache between the two refs"
                                 " if ref2 is missing, 'master' is assumed")
        return parser

    def load_issues(self, issue_ids: Iterable[int]) -> Iterable[dict]:
        if self.args.parallel:
            cur, total = (int(x) for x in self.args.parallel.split("/", 2))
            issue_ids = [x for x in issue_ids if (x % total) == (cur - 1)]

        for number in self.show_progress(issue_ids):
            if self.redmine.issues.storage.get(str(number), None) is None:
                continue
            yield self.redmine.issues.issue(number)

    def run(self):
        """
        Upload Redmine data to Gitlab
        """
        self.redmine = Redmine(self.settings)
        self.gitlab = GitlabProject(self.settings)
        self.converter = Converter(self.redmine, self.gitlab, self.settings)
        for p in self.redmine.projects.list():
            if p["identifier"] == self.settings.GITLAB_PROJECT:
                project = p
                break
        else:
            raise RuntimeError(f"project {self.settings.GITLAB_PROJECT} not found")

        log.info("Redmine project: %r", project)

        if not self.args.first_run and not self.args.second_run:
            log.warning('Neither --first_run nor --second_run is specified. Fall back to --first_run')
            self.args.first_run = True

        if self.args.progress:
            from .utils import progress
            self.show_progress = progress
        else:
            self.show_progress = lambda x: x

        issues = self.args.issues
        if self.args.changed_issues:
            from .discuss import Discuss
            discuss = Discuss(self.args)
            issues = sorted(set(discuss.list_changed_tickets(self.args.changed_issues)), reverse=True)
        elif not issues:
            issues = sorted(self.redmine.all_our_issue_ids(), reverse=True)

        issues = [i for i in issues if self.redmine.issues.issue(i)["project"]["id"] == project["id"]]

        if self.args.list_issues:
            for issue in issues:
                print(issue)
            return

        resume = None
        if self.args.resume:
            resume = ResumeFile(self.args.resume)
            issues = [x for x in issues if x < resume.last]

        if self.args.first_run:
            log.info("Running first run")
            for issue in self.load_issues(issues):
                self.converter.create(issue)
                if resume:
                    resume.set_last(issue["id"])

        # second run to fix all links between issues
        if self.args.second_run:
            log.info("Running second run")
            self.converter.set_second_run(True)
            for issue in self.load_issues(issues):
                self.converter.update(issue)
                if resume:
                    resume.set_last(issue["id"])
