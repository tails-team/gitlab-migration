from .cmdline import Command
from .redmine import Redmine


class Download(Command):
    """
    Download the contents of a redmine repository and store it as json files
    """
    @classmethod
    def make_subparser(cls, subparsers):
        parser = super().make_subparser(subparsers)
        return parser

    def run(self):
        """
        Visit all redmine things to prefetch them locally
        """
        is_admin = self.settings.REDMINE_IS_ADMIN
        redmine = Redmine(self.settings)

        # Admin-only data downloads
        if is_admin:
            redmine.custom_field_list()
            redmine.user_list()
            redmine.group_list()

        redmine.query_list()
        redmine.tracker_list()
        redmine.issue_priority_list()
        redmine.document_category_list()
        role_ids = sorted(role["id"] for role in redmine.roles_list())
        for number in reversed(role_ids):
            redmine.role(number)

        project_ids = redmine.project_ids()
        for project_id in reversed(project_ids):
            redmine.projects.project(project_id)
            redmine.projects.membership(project_id)
            if is_admin:
                redmine.projects.issue_categories(project_id)
                redmine.projects.versions(project_id)

            for issue_id in reversed([x["id"] for x in redmine.issues.list(project_id)]):
                issue = redmine.issues.issue(issue_id)
                redmine.issues.relations(issue_id)
                if not redmine.settings.REDMINE_SKIP_ATTACHMENTS:
                    for a in issue["attachments"]:
                        redmine.attachment(a["id"], a["content_url"])

        redmine.issues.statuses()

        if is_admin:
            user_ids = sorted(i["id"] for i in redmine.user_list())
            for number in reversed(user_ids):
                redmine.user(number)
