from .cmdline import Command, Fail
from .converter import Converter
from .gitlab import GitlabProject, APIException
from .redmine import Redmine
import logging
import re

log = logging.getLogger(__name__)


class Update(Command):
    """
    Post-migration update procedures
    """

    @classmethod
    def make_subparser(cls, subparsers):
        parser = super().make_subparser(subparsers)
        parser.add_argument("--user-emails", action="store_true",
                            help="update verified user emails")
        parser.add_argument("--force-set", action="store_true",
                            help="update the mail even if it had already been updated")
        parser.add_argument("--fix-stub-issues", action="store_true",
                            help="will create all missing stub issues")
        parser.add_argument("--fix-links-to-notes", action="store_true",
                            help="will fix the links to notes of other issues.")
        parser.add_argument("--fix-images-links", action="store_true",
                            help="will fix the links of images.")
        parser.add_argument("--fix-unsecure-links", action="store_true",
                            help="will fix usecure http links to redmine.")
        parser.add_argument("--empty-todo", action="store_true",
                            help="Empty users' To-Do list.")
        parser.add_argument("--add_star_for_watched_issues", action="store_true",
                            help="Add a star emoji for watched issues.")
        return parser

    def update_user_emails(self):
        """
        Remove user@example.org email address for all users.

        Add the email from redmine instead, as a verified email.
        """
        # https://docs.gitlab.com/ee/api/users.html#add-email-for-user
        redmine = Redmine(self.settings)
        gitlab = GitlabProject(self.settings)
        force = self.args.force_set

        # Map Redmine unsernames to user emails
        email_by_name = {}
        # No need to filter out inactive/spam users, since we take the master
        # user list from gitlab
        # for user_id in redmine.active_user_ids():
        #     user = redmine.user(user_id)
        for user in redmine.user_list():
            email = user.get("mail")
            if email is None:
                log.warn("User without email: %r", user)
                continue
            # Reapply the username mapping we use when creating them on gitlab
            name = user["login"].replace("@", "_")
            # replace username, that does not fit gitlab username rules
            if name.endswith("."):
                name += "_"
            email_by_name[name] = user["mail"]

        # now = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        users = gitlab.api.get_everything('users')
        for user in users:
            payload = {}

            # The primary email is not returned in /users/:id/emails API endpoint.
            # See: https://gitlab.com/gitlab-org/gitlab-foss/-/issues/53618
            # print(user)
            if force or user["email"].endswith("@example.com"):
                email = email_by_name.get(user["name"])
                if email is None:
                    log.error("No email mapping for user %s", user["name"])
                    continue
                # Without skip_reconfirmation=True, the change does not happen
                # and the user is mailed a confirmation mail instead
                payload["email"] = email
                payload["skip_reconfirmation"] = True
                log.info("%s: mail set to %s", user["name"], email)
            else:
                log.info("%s: already migrated", user["name"])

            # if not user.get("confirmed_at"):
            #     log.info("%s: confirming account", user["name"])
            #     payload["confirmed_at"] = now
            #     payload["confirm"] = "false"
            #     # import requests
            #     # res = requests.put(f"http://gitlab.example.org/admin/users/{user['name']}/confirm", headers=gitlab.api.headers)
            #     # print(res)

            if payload:
                res = gitlab.api.put(f'/users/{user["id"]}', json=payload, return_json=False)
                if res.status_code != 200:
                    log.error("%s: failed to update with %r: %d %s",
                              user["name"], payload, res.status_code, res.reason)

            # Check if there's a leftover example.com email in the other email
            # list
            for e in gitlab.api.get_everything(f'/users/{user["id"]}/emails'):
                if e["email"].endswith("@example.com"):
                    log.info("%s: removing old email %s", user["name"], e["email"])
                    res = gitlab.api.delete(f"/users/{user['id']}/emails/{e['id']}", return_json=False)
                    if res.status_code != 204:
                        log.error("%s: failed to remove email %s (%d %s)",
                                  user["name"], e["email"], res.status_code, res.reason)

    def fix_stub_issues(self):
        """
        Create missing stub issues.
        """
        gitlab = GitlabProject(self.settings)
        redmine = Redmine(self.settings)
        converter = Converter(redmine, gitlab, self.settings)

        for name, issue in gitlab.issues.elements():
            iid = issue['iid']
            if issue["project"] != self.settings.GITLAB_PROJECT:
                if not gitlab.stub_issues.get(str(iid), None):
                    log.info(f"stub issue missing for #{iid}")
                    stub_issue = converter.stub_issue(issue)
                    gitlab.create_or_update_stub_issue(stub_issue)
                    # the second run will close the ticket correctly
                    gitlab.create_or_update_stub_issue(stub_issue)

    def fix_links_to_notes(self):
        """
        Previous we used a style to link to notes, that does not work on production.
        Use the whole url to point to fix this.
        """
        gitlab = GitlabProject(self.settings)

        re_note_link = re.compile(r"\[tails/[^#]+#[0-9]+-note_[0-9]+\]\((/tails/[^\)]+/)(issues/[0-9]+[^\)]+)\)")

        for name, issue in gitlab.issues.elements():
            iid = issue['iid']
            m = re_note_link.search(issue['description'])
            if m:
                log.info(f"found old link to notes in #{iid}")
                issue['description'] = re_note_link.sub(rf"{self.settings.GITLAB_URL}\g<1>-/\g<2>", issue['description'])
                issue['author'] = issue['author']['username']
                gitlab.update_issue(issue)
            for index, comment in enumerate(gitlab.comment(iid)):
                m = re_note_link.search(comment['body'])
                if m:
                    log.info(f"found old link to notes in #{iid} comment {index}")
                    comment['body'] = re_note_link.sub(rf"{self.settings.GITLAB_URL}\g<1>-/\g<2>", comment['body'])
                    comment['author'] = comment['author']['username']
                    gitlab.update_comment(iid, index, comment)

    def fix_images_links(self):
        """
        Update image links to point to something that exists.
        """
        gitlab = GitlabProject(self.settings)
        redmine = Redmine(self.settings)
        converter = Converter(redmine, gitlab, self.settings)

        re_image_link = re.compile(r"(!\[.*?\])\((.*?)\)")

        for name, issue in gitlab.issues.elements():
            iid = issue['iid']
            redmine_issue = redmine.issues.issue(iid)

            def repl(m):
                url = converter.formatter.resolve_image_url(redmine_issue, m.group(2))
                return f"{m.group(1)}({url})"

            for index, comment in enumerate(gitlab.comment(iid)):
                m = re_image_link.search(comment['body'])
                if not m:
                    continue
                comment['body'] = re_image_link.sub(repl, comment['body'])
                comment['author'] = comment['author']['username']
                gitlab.update_comment(iid, index, comment)

            m = re_image_link.search(issue['description'])
            if not m:
                continue
            issue['description'] = re_image_link.sub(repl, issue['description'])
            issue['author'] = issue['author']['username']
            gitlab.update_issue(issue)

    def fix_unsecure_links(self):
        """
        fix usecure http links to redmine.
        """
        gitlab = GitlabProject(self.settings)

        re_attachment_url = re.compile(
            r"http://(?:labs.riseup.net|redmine.tails.boum.org)/code/attachments/(?:download/)")

        for name, issue in gitlab.issues.elements():
            iid = issue['iid']
            text = issue['description']
            m = re_attachment_url.search(text)
            if m:
                log.info(f"found http link in #{iid}")
                text = re_attachment_url.sub(f"https://redmine.tails.boum.org/code/attachments/download/", text)
                issue['description'] = text
                issue['author'] = issue['author']['username']
                gitlab.update_issue(issue)
            for index, comment in enumerate(gitlab.comment(iid)):
                text = comment['body']
                m = re_attachment_url.search(text)
                if m:
                    log.info(f"found http link in #{iid} comment {index}")
                    text = re_attachment_url.sub(f"https://redmine.tails.boum.org/code/attachments/download/", text)
                    comment['body'] = text
                    comment['author'] = comment['author']['username']
                    gitlab.update_comment(iid, index, comment)

    def empty_todo(self):
        """
        Empty users' To-Do list.
        """

        gitlab = GitlabProject(self.settings)
        users = gitlab.api.get_everything('users')
        for user in users:
            log.info(f"Empty {user['name']}s' To-Do list.")
            ret = gitlab.api.post(f"todos/mark_as_done?sudo={user['id']}", return_json=False)
            if ret.status_code != 204:
                raise APIException(ret.text)

    def add_star_for_watched_issues(self):
        """
        Add a star emoji for watched issues.
        """
        gitlab = GitlabProject(self.settings)
        redmine = Redmine(self.settings)

        for number in redmine.all_our_issue_ids():
            redmine_issue = redmine.issues.issue(number)
            gitlab_issue = gitlab.issues.get(str(number))

            # Remove all stars from the issue
            gitlab.remove_reactions(gitlab_issue, "star")

            for watcher in redmine_issue.get("watchers", []):
                if isinstance(watcher, (int, str)):
                    user_id = int(watcher)
                    redmine_user = redmine.user_or_group(user_id)
                else:
                    redmine_user = redmine.user_or_group(watcher['id'])

                if redmine_user is None:
                    continue

                gitlab_user = gitlab.user_create_if_needed(redmine_user["login"], {})

                gitlab.add_reaction(gitlab_issue, gitlab_user, {'name': 'star'})

    def run(self):
        if self.args.user_emails:
            self.update_user_emails()
        elif self.args.fix_stub_issues:
            self.fix_stub_issues()
        elif self.args.fix_links_to_notes:
            self.fix_links_to_notes()
        elif self.args.empty_todo:
            self.empty_todo()
        elif self.args.add_star_for_watched_issues:
            self.add_star_for_watched_issues()
        elif self.args.fix_images_links:
            self.fix_images_links()
        elif self.args.fix_unsecure_links:
            self.fix_unsecure_links()
        else:
            raise Fail("please use a command line switch to select what to do. See --help")
