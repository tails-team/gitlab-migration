# Working directory containing intermediate files
WORKDIR = "."

# Working directory containing gitlabracadabra files
GITLAB_CONFIG = "../gitlab-config"

# URL to the redmine site to read
REDMINE_URL = None

# Redmine API key
REDMINE_API_KEY = None

# True if the user is a redmine admin
REDMINE_IS_ADMIN = True

# Do not download redmine attachments
REDMINE_SKIP_ATTACHMENTS = False

# Limit import to projects with these names
REDMINE_PROJECTS_INCLUDE = None

# Extra information for redmine lookups
REDMINE_EXTRA_INFO = {}

# URL to the gitlab repository to write
GITLAB_URL = None

# Base URL of the public archive
ARCHIVE_PUBLIC_URL = "https://public.example.org/redmine"

# Base URL of the private archive
ARCHIVE_PRIVATE_URL = "https://private.example.org/redmine"

# URL to the Gitolite repository in Tails infrastructure
GITOLITE_URL = 'ssh://gitolite@git.puppet.tails.boum.org:3004'

# Create remote mirrors for the following projects in the Tails infrastructure
MIRRORS = [
    'tails',
    'etcher-binary',
    'mirror-pool',
    'mirror-pool-dispatcher',
    'promotion-material',
]
