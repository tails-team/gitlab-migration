import jinja2
import os


class Templates:
    def __init__(self, formatter, site_root=".", autoescape=True):
        self.redmine = formatter.redmine
        self.formatter = formatter
        self.jinja2 = jinja2.Environment(
            loader=jinja2.FileSystemLoader([
                os.path.join(site_root, "templates"),
            ]),
            autoescape=autoescape,
        )
        self.jinja2.globals["issue_subtasks"] = self.issue_subtasks
        self.jinja2.globals["issue_journals"] = self.issue_journals
        self.jinja2.globals["issue_relations"] = self.issue_relations
        self.jinja2.globals["issue_has_relations"] = self.issue_has_relations
        self.jinja2.globals["journal_details"] = self.journal_details
        self.jinja2.globals["journal_has_details"] = self.journal_has_details
        self.jinja2.globals["has_subtasks"] = self.has_subtasks
        self.jinja2.filters["user"] = self.formatter.format_user
        self.jinja2.filters["issue"] = self.formatter.format_issue
        self.jinja2.filters["project"] = self.formatter.format_project
        self.jinja2.filters["status"] = self.formatter.format_status
        self.jinja2.filters["tracker"] = self.formatter.format_tracker
        self.jinja2.filters["priority"] = self.formatter.format_priority
        self.jinja2.filters["category"] = self.formatter.format_category
        self.jinja2.filters["version"] = self.formatter.format_version
        self.jinja2.filters["attachment"] = self.formatter.format_attachment

    def issue_subtasks(self, issue):
        for subissue in issue.get("children", ()):
            subissue = self.redmine.issues.issue(subissue["id"])
            if not self.formatter.private and subissue.get("is_private"):
                continue
            yield subissue

    def issue_relations(self, issue):
        for rel in issue.get("relations", ()):
            if not isinstance(rel["other"], dict):
                continue
            if not self.formatter.private and rel["other"].get("is_private"):
                continue
            yield rel

    def issue_has_relations(self, issue):
        for issue in self.issue_relations(issue):
            return True
        return False

    def issue_journals(self, issue):
        for entry in issue.get("journals", ()):
            if not self.formatter.private and entry.get("private_notes"):
                continue
            yield entry

    def journal_details(self, event):
        for detail in event.get("details", ()):
            if not self.formatter.private and detail.get("private_issue"):
                continue
            yield detail

    def journal_has_details(self, event):
        for detail in self.journal_details(event):
            return True
        return False

    def has_subtasks(self, issue):
        for issue in self.issue_subtasks(issue):
            return True
        return False

    def get(self, template_name):
        return self.jinja2.get_template(template_name)
