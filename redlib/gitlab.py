from __future__ import annotations
from typing import Any, Dict, List, Iterable, Callable, Optional, Union
import time
import random
import copy
import logging
from functools import partial
import os
import secrets
import requests
import socket
import urllib

from .cmdline import Fail
from .settings import Settings
from .utils import Workdir

log = logging.getLogger(__name__)


class APIException(Exception):
    pass


def get_everything(func, url, *args, **kw):
    """Gitlab API paginate everything, so we need to deal with it and request every page for the request"""

    if "?" in url:
        _url = url + "&per_page=100"
    else:
        _url = url + "?per_page=100"

    _r = func(_url, *args, return_json=False, **kw)
    if _r.status_code != 200:
        raise APIException(_r.text)

    yield from _r.json()

    total_pages = int(_r.headers.get('X-Total-Pages', '1'))
    for page in range(2, total_pages+1):
        _r = func(_url+f"&page={page}", *args, return_json=False, **kw)
        if _r.status_code != 200:
            raise APIException(_r.text)
        yield from _r.json()


class NoRecoverException(Exception):
    pass


def recoverFunc():
    raise NoRecoverException()


def retry(count, recoverFunc=recoverFunc):
    def decorator(f):
        def func(*args, **kwargs):
            exception = None
            for i in range(count):
                try:
                    return f(*args, **kwargs)
                except socket.timeout as e:
                    exception = e
                    try:
                        return recoverFunc()
                    except NoRecoverException:
                        time.sleep(random.randint(400, 800)/1000)
            else:
                raise exception
        return func
    return decorator


retry_func = retry(5)


class GitLabClient:
    def __init__(self, url, key):
        self.project_id = None
        self.url = url
        self.headers = {'PRIVATE-TOKEN': key}
        self.projects_url = "projects"
        self.session = requests.Session()

    def api_url(self, *parts):
        return urllib.parse.urljoin(self.url, "/".join(['api/v4', *parts]))

    def get(self, path, return_json=True, *args, **kwargs):
        ret = retry_func(self.session.get)(self.api_url(path), *args, headers=self.headers, **kwargs)
        if return_json:
            return ret.json()
        return ret

    def get_everything(self, path, *args, **kwargs):
        yield from get_everything(self.get, path, *args, **kwargs)

    def post(self, path, return_json=True, *args, **kwargs):
        ret = retry_func(self.session.post)(self.api_url(path), *args, headers=self.headers, **kwargs)
        if return_json:
            return ret.json()
        return ret

    def put(self, path, return_json=True, *args, **kwargs):
        ret = retry_func(self.session.put)(self.api_url(path), *args, headers=self.headers, **kwargs)
        if return_json:
            return ret.json()
        return ret

    def delete(self, path, return_json=True, *args, **kwargs):
        ret = retry_func(self.session.delete)(self.api_url(path), *args, headers=self.headers, **kwargs)
        if return_json:
            return ret.json()
        return ret

    def project_get(self, path, project=None, return_json=True, *args, **kwargs):
        if project is None:
            project_id = self.project_id
        else:
            project_id = project['id']
        ret = retry_func(self.session.get)(self.api_url(
            self.projects_url, str(project_id), path), *args, headers=self.headers, **kwargs)
        if return_json:
            return ret.json()
        return ret

    def project_post(self, path, project=None, return_json=True, *args, **kwargs):
        if project is None:
            project_id = self.project_id
        else:
            project_id = project['id']
        ret = retry_func(
                self.session.post)(self.api_url(
                    self.projects_url, str(project_id), path), *args, headers=self.headers, **kwargs)
        if return_json:
            return ret.json()
        return ret

    def project_put(self, path, project=None, return_json=True, *args, **kwargs):
        if project is None:
            project_id = self.project_id
        else:
            project_id = project['id']
        ret = retry_func(
                self.session.put)(self.api_url(
                    self.projects_url, str(project_id), path), *args, headers=self.headers, **kwargs)
        if return_json:
            return ret.json()
        return ret

    def project_delete(self, path, project=None, return_json=True, *args, **kwargs):
        if project is None:
            project_id = self.project_id
        else:
            project_id = project['id']
        ret = retry_func(self.session.delete)(self.api_url(
            self.projects_url, str(project_id), path), *args, headers=self.headers, **kwargs)
        if return_json:
            return ret.json()
        return ret


class GitlabProject:
    def __init__(self, settings: Settings):
        if settings.GITLAB_URL is None:
            raise Fail("GITLAB_URL is not set in the configuration")

        if settings.GITLAB_API_KEY is None:
            raise Fail("GITLAB_API_KEY is not set in the configuration")
        root = os.path.join(settings.WORKDIR, "gitlab")
        self.issues = Workdir(os.path.join(root, "issues"), cache=True)
        self.stub_issues = Workdir(os.path.join(root, "stub_issues"), cache=True)
        self.comments = Workdir(os.path.join(root, "comments"), cache=True)
        self.attachments = Workdir(os.path.join(root, "attachments"), cache=True)
        self.labels = Workdir(os.path.join(root, "labels"), cache=True)
        self.milestones = Workdir(os.path.join(root, "milestones"), cache=True)
        self.users = Workdir(os.path.join(root, "users"), cache=True)
        self.projects = Workdir(os.path.join(root, "projects"), cache=True)

        self.namespace_api = GitLabClient(settings.GITLAB_URL, settings.GITLAB_API_KEY)
        self.namespace_api.group_name = settings.GITLAB_NAMESPACE
        self.namespace_api.project = self.group(settings.GITLAB_NAMESPACE)
        self.namespace_api.project_id = self.namespace_api.project['id']
        self.namespace_api.projects_url = 'groups'

        self.api = GitLabClient(settings.GITLAB_URL, settings.GITLAB_API_KEY)
        self.api.project_path = settings.GITLAB_PROJECT
        self.api.project = self.project_create_if_needed(settings.GITLAB_PROJECT)
        self.api.project_id = self.api.project['id']

    def issues_url(self):
        return urllib.parse.urljoin(self.api.url, "/".join([self.api.project_path, 'issues']))

    def group(self, name):
        groups = self.namespace_api.get('groups')
        for group in groups:
            if group['name'] == name:
                return group
        else:
            data = {
                "name": self.namespace_api.group_name,
                "path": self.namespace_api.group_name,
                "visibility": "public",
            }
            return self.namespace_api.post('groups', data=data)

    def project(self, projectNameWithNamespace: str):
        project = self.projects.get(projectNameWithNamespace, None)
        if project:
            return project

        projects = get_everything(partial(self.api.get, data={'simple': True}), 'projects')

        ret = None
        for project in projects:
            self.projects.set(project['path_with_namespace'], project)
            if project['path_with_namespace'] == projectNameWithNamespace:
                ret = project
        return ret

    def fix_issue(self, issue_id: int, project: Union[Dict[Any, Any], int], notexisting=False, stub=False) -> None:
        if type(project) == int:
            _ret = self.api.get(f"projects/{project}", data={'simple': True}, return_json=False)
            if _ret.status_code != 200:
                raise APIException(_ret.text)

            project = _ret.json()

        _ret = self.api.project_get(f'issues/{issue_id}', project=project, return_json=False)

        if _ret.status_code != 200:
            if notexisting:
                return True
            else:
                raise APIException(_ret.text)

        issue = _ret.json()
        issue['project'] = project['path_with_namespace']
        if stub:
            self.stub_issues.set(str(issue_id), issue)
            return

        self.issues.set(str(issue_id), issue)
        try:
            comments_url = f'issues/{issue_id}/notes?sort=asc'
            _ret = get_everything(partial(self.api.project_get, project=project), comments_url)
            comments = [i for i in _ret if not i['system']]
            self.comments.set(str(issue_id), comments)
        except APIException:
            log.error("Download of comments for #{issue_id} failed.")

    def create_issue(self, issue_id: int, func: Callable, stub=False) -> Dict[Any, Any]:
        number = issue_id
        issue = self.issue(number)

        if stub:
            issue = self.stub_issue(number)

        if issue is None:
            data = func()
            project = self.project(data.get("project"))
            self.fix_issue(number, project, notexisting=True, stub=stub)
            if stub:
                issue = self.stub_issue(number)
            else:
                issue = self.issue(number)

        if issue is not None:
            return issue

        if stub:
            log.info("create stub issue #%i", number)
        else:
            log.info("create issue #%i", number)
        url = "issues"
        author = data.get("author")
        del data['author']
        del data['project']
        if author:
            user = self.user_create_if_needed(author, data={})
            if user:
                url += f"?sudo={user['id']}"
        ret = self.api.project_post(url, project=project, json=data, return_json=False)
        if ret.status_code != 201:
            raise Exception(ret.text)
        issue = ret.json()
        issue['project'] = project['path_with_namespace']
        if stub:
            self.stub_issues.set(str(number), issue)
        else:
            self.issues.set(str(number), issue)
        return issue

    def update_issue(self, data: Dict[Any, Any], stub=False) -> Dict[Any, Any]:
        number = data['iid']
        if stub:
            log.info("update stub issue #%i", number)
        else:
            log.info("update issue #%i", number)
        url = f'issues/{data["iid"]}'
        author = data.get("author")
        project = self.project(data.get("project"))
        data = copy.copy(data)
        del data['author']
        del data['project']
        if author:
            user = self.user_create_if_needed(author, data={})
            if user:
                url += f"?sudo={user['id']}"
        ret = self.api.project_put(url, project=project, json=data, return_json=False)
        if ret.status_code != 200:
            raise Exception(ret.text)
        issue = ret.json()
        issue['project'] = project['path_with_namespace']
        if stub:
            self.stub_issues.set(str(number), issue)
        else:
            self.issues.set(str(number), issue)
        return issue

    def create_or_update_stub_issue(self, data: Dict[Any, Any]) -> Dict[Any, Any]:
        number = data['iid']
        if self.stub_issue(number):
            return self.update_issue(data, stub=True)
        return self.create_issue(number, lambda: copy.copy(data), stub=True)

    def issue(self, number: int) -> Dict[Any, Any]:
        issue = self.issues.get(str(number), None)
        if issue:
            if not issue.get('project', None):
                self.fix_issue(number, issue['project_id'])
                issue = self.issues.get(str(number), None)
        return issue

    def stub_issue(self, number: int) -> Dict[Any, Any]:
        return self.stub_issues.get(str(number), None)

    def comment(self, issue_number: int) -> List[Dict[Any, Any]]:
        comments = self.comments.get(str(issue_number), [])
        if comments:
            issue = self.issue(issue_number)
            for c in self.comments.get(str(issue_number), []):
                if not c.get('id', None):
                    self.fix_issue(issue_number, issue['project_id'])
                    break
        return self.comments.get(str(issue_number), [])

    def create_comments(self, issue_number: int, count: int, func: Callable) -> List[Dict[Any, Any]]:
        comments = self.comments.get(str(issue_number), [])
        issue = self.issue(issue_number)

        if len(comments) > count:
            return comments

        if issue.get('project', None) is None:
            log.warning(f'#{issue_number} has no project set - that is weird.')
            self.fix_issue(issue_number, issue['project_id'])
            issue = self.issue(issue_number)
            comments = self.comments.get(str(issue_number), [])
            if len(comments) > count:
                return comments
            if not issue.get('project', None):
                raise APIException(f"could not fix the project entry for #{issue_number}")

        for comment in func()[len(comments):]:
            log.info("create comment %i for issue %i", len(comments), issue_number)
            url = f'issues/{issue_number}/notes'
            author = comment.get("author")
            if comment.get("author"):
                user = self.user_create_if_needed(author, data={})
                if user:
                    url += f"?sudo={user['id']}"
            ret = self.api.project_post(url, project=self.project(issue['project']), json=comment, return_json=False)
            if ret.status_code != 201:
                raise Exception(ret.text)
            comments.append(ret.json())
        self.comments.set(str(issue_number), comments)
        return comments

    def update_comment(
            self, issue_number: int, index: int, comment: Dict[Any, Any], update_cache=True) -> Dict[Any, Any]:
        issue = self.issue(issue_number)
        log.info("update comment %i for issue %i", index, issue_number)
        url = f'issues/{issue_number}/notes/{comment["id"]}'
        author = comment.get("author")
        if comment.get("author"):
            user = self.user_create_if_needed(author, data={})
            if user:
                url += f"?sudo={user['id']}"
        ret = self.api.project_put(url, project=self.project(issue['project']), json=comment, return_json=False)
        if ret.status_code != 200:
            raise Exception(ret.text)
        _comment = ret.json()
        if update_cache:
            comments = self.comments.get(str(issue_number), [])
            comments[index] = _comment
            self.comments.set(str(issue_number), comments)
        return _comment

    def update_comments(self, issue_number: int, data: List[Dict[Any, Any]]) -> List[Dict[Any, Any]]:
        comments = self.comments.get(str(issue_number), [])
        # Make sure the issue exists
        self.issue(issue_number)

        if not comments:
            raise Fail("You can't update a non existing comments")

        for i, comment in enumerate(data):
            comment['id'] = comments[i]['id']
            comments[i] = self.update_comment(issue_number, i, comment, update_cache=False)

        self.comments.set(str(issue_number), comments)
        return comments

    def create_attachment(self, attachment_id: int, filepath: str) -> Dict[Any, Any]:
        attachments = self.attachments.get('__attachments__', {})

        if attachments and str(attachment_id) in attachments:
            attachment = attachments[str(attachment_id)]
        else:
            log.info("uploads attachment %i", attachment_id)
            files = {"file": open(filepath, 'rb')}
            attachment = self.api.project_post('uploads', files=files)
            attachments[str(attachment_id)] = attachment
            self.attachments.set('__attachments__', attachments)
        return attachment

    def subscribe_user(self, issue, user) -> None:
        project = self.project(issue['project'])
        url = f"issues/{issue['iid']}/subscribe?sudo={user['id']}"
        ret = self.api.project_post(url, project=project, return_json=False)
        if ret.status_code not in (201, 304):
            raise Exception(ret.text)

    def remove_reactions(self, issue: Dict[str, Any], name: str) -> None:
        """
        Remove the existing reactions with the given name from the issue
        """
        project = self.project(issue['project'])
        # Query existing emojis
        url = f"issues/{issue['iid']}/award_emoji"
        ret = self.api.project_get(url, project=project, return_json=False)
        if ret.status_code != 200:
            raise Exception(ret.text)
        for entry in ret.json():
            if entry["name"] == name:
                delete_url = f"issues/{issue['iid']}/award_emoji/{entry['id']}"
                ret = self.api.project_delete(delete_url, project=project,  return_json=False)
        if ret.status_code not in (200, 202, 204, 304):
            raise Exception(ret.text)

    def add_reaction(self, issue, user, data) -> None:
        project = self.project(issue['project'])

        url = f"issues/{issue['iid']}/award_emoji?sudo={user['id']}"
        ret = self.api.project_post(url, project=project, data=data, return_json=False)
        if ret.status_code == 404:
            log.warning("Cannot add reaction %r to issue %r for user %r: %s",
                        data, issue['iid'], user['id'], ret.text)
            return
        if ret.status_code not in (201, 304):
            raise Exception(ret.text)

    def attachment(self, attachment_id: int) -> Dict[Any, Any]:
        attachments = self.attachments.get('__attachments__', {})
        return attachments.get(str(attachment_id), None)

    def label_create_if_needed(self, name: str, color: str):
        labels = self.labels.get("__labels__", None)
        if not labels:
            log.info("download labels list")
            labels = self.namespace_api.project_get('labels')
            self.labels.set('__labels__', labels)
        try:
            label = [i for i in labels if i['name'] == name][0]
        except IndexError:
            data = {'name': name,
                    'color': color,
                    }
            _label = self.namespace_api.project_get(f'labels/{name}', return_json=False)
            if _label.status_code == 200:
                labels.append(_label.json())
                self.labels.set('__labels__', labels)
                return
            log.info("create label %s", name)
            label = self.namespace_api.project_post('labels', json=data, return_json=False)
            if label.status_code == 201:
                labels.append(label.json())
                self.labels.set('__labels__', labels)

        return label

    def project_create_if_needed(self, path_with_name_space: str):
        project = self.project(path_with_name_space)
        if project:
            return project

        data = {
                'name': path_with_name_space.split("/")[-1],
                'namespace_id': self.namespace_api.project_id,
                'issues_enabled': True,
                'visibility': 'public',
        }

        project = self.api.post('projects', data=data)
        self.projects.set(path_with_name_space, project)
        return project

    def milestone_create_if_needed(self, title: str):
        milestone = self.milestones.get(title, None)

        if not milestone:
            milestones = self.milestones.get("__milestones__", None)
            if not milestones:
                log.info("download milestones list")
                milestones = self.namespace_api.project_get('milestones?per_page=100')
                self.milestones.set('__milestones__', milestones)
            try:
                milestone = [i for i in milestones if i['title'] == title][0]
            except IndexError:
                data = {'title': title,
                        }
                log.info("create milestones %s", title)

                _milestone = self.namespace_api.project_post('milestones', json=data, return_json=False)
                if _milestone.status_code != 201:
                    raise Exception(_milestone.text)
                milestone = _milestone.json()

                self.milestones.set(title, milestone)

                milestones.append(milestone)
                self.milestones.set('__milestones__', milestones)

        return milestone

    def user_create_if_needed(self, name: str, data: Dict[str, str]):
        users = self.users.get("__users__", None)
        if not users:
            log.info("download users list")
            users = self.api.get('users')
            self.users.set('__users__', users)
        name = name.replace("@", "_")

        # replace username, that does not fit gitlab username rules
        if name.endswith("."):
            name += "_"
        user = self.users.get(name, None)
        if not user:
            _data = {
                        'username': name,
                        'password': secrets.token_urlsafe(32),
                        'name': name,
                        'email': f'{name}@example.com',
                    }
            _data.update(data)
            username = urllib.parse.quote_plus(name.lower())
            user = self.api.get(f'users?username={username}', return_json=False)
            if user.status_code != 200:
                user = None
            else:
                r = user.json()
                if r:
                    user = r[0]
                else:
                    user = None

            if not user:
                log.info("create user %s", name)
                user = self.api.post('users', json=_data, return_json=False)
                if user.status_code != 201:
                    raise Exception(user.text)
                else:
                    user = user.json()
            if user:
                self.users.set(name, user)
        if user:
            self.user_group_permission(user)
        return user

    def user_group_permission(self, user):
        member = self.users.get(user['username']+"_permission", {})
        max_level = 50
        if self.api.project['namespace']['kind'] == "user":
            max_level = 40
        if member.get('access_level', 0) < max_level:
            _member = {'user_id': user['id'], 'access_level': max_level}
            if self.api.project['namespace']['kind'] == "user":
                get_func = self.api.project_get
                url = 'members'
                if member:
                    url += f"/{user['id']}"
                    func = self.api.project_put
                else:
                    func = self.api.project_post
            elif self.api.project['namespace']['kind'] == "group":
                get_func = self.api.get
                url = '/'.join(['groups', self.api.project['namespace']['path'], 'members'])
                if member:
                    url += f"/{user['id']}"
                    func = self.api.put
                else:
                    func = self.api.post
            ret = func(url, json=_member, return_json=False)
            if ret.status_code in (200, 201):
                self.users.set(user['username']+"_permission", ret.json())
                return
            elif ret.status_code == 409 and not member:
                ret = get_func(f"{url}/{user['id']}", return_json=False)
                if ret.status_code == 200:
                    self.users.set(user['username']+"_permission", ret.json())
                    self.user_group_permission(user)
                    return

    def add_label_to_issues(
            self, label: str, issues: Iterable[id], author: Optional[str] = None, color: str = "#17A2B8"):
        """
        Add a label to the given issues
        """
        if author is None:
            from .converter import MIGRATION_USER
            author = MIGRATION_USER

        self.label_create_if_needed(label, color)

        for issue_id in issues:
            issue = self.issue(issue_id)
            if issue is None:
                log.error("Issue #%d not available", issue_id)
                continue
            labels = issue.get("labels", ())
            if labels is None:
                labels = set()
            else:
                labels = set(labels)
            if label in labels:
                continue
            labels.add(label)
            issue["labels"] = sorted(labels)
            issue["author"] = author
            issue.pop("updated_at", None)
            self.update_issue(issue)
