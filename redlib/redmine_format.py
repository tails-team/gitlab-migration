import operator
from typing import Any, Dict, Union
import logging

log = logging.getLogger()


RELATION_NAMES = {
    "relates": ["Related to", "Related to"],
    "duplicates": ["Is duplicate of", "Has duplicate"],
    "duplicated": ["Has duplicate", "Is duplicate of"],
    "blocks": ["Blocks", "Blocked by"],
    "blocked": ["Blocked by", "Blocks"],
    "precedes": ["Precedes", "Follows"],
    "follows": ["Follows", "Precedes"],
    "copied_to": ["Copied to", "Copied from"],
    "copied_from": ["Copied from", "Copied to"],
}


FIELD_LABELS = {
    "name": "Name",
    "description": "Description",
    "summary": "Summary",
    "is_required": "Required",
    "firstname": "First name",
    "lastname": "Last name",
    "mail": "Email",
    "address": "Email",
    "filename": "File",
    "filesize": "Size",
    "downloads": "Downloads",
    "author": "Author",
    "created_on": "Created",
    "updated_on": "Updated",
    "closed_on": "Closed",
    "field_format": "Format",
    "is_for_all": "For all projects",
    "possible_values": "Possible values",
    "regexp": "Regular expression",
    "min_length": "Minimum length",
    "max_length": "Maximum length",
    "value": "Value",
    "category": "Category",
    "title": "Title",
    "project": "Project",
    "issue": "Issue",
    "status": "Status",
    "notes": "Notes",
    "is_closed": "Issue closed",
    "is_default": "Default value",
    "tracker": "Tracker",
    "subject": "Subject",
    "due_date": "Due date",
    "assigned_to": "Assignee",
    "priority": "Priority",
    "fixed_version": "Target version",
    "user": "User",
    "principal": "Principal",
    "role": "Role",
    "homepage": "Homepage",
    "is_public": "Public",
    "parent": "Subproject of",
    "is_in_roadmap": "Issues displayed in roadmap",
    "login": "Login",
    "mail_notification": "Email notifications",
    "admin": "Administrator",
    "last_login_on": "Last connection",
    "language": "Language",
    "effective_date": "Due date",
    "password": "Password",
    "new_password": "New password",
    "password_confirmation": "Confirmation",
    "version": "Version",
    "type": "Type",
    "host": "Host",
    "port": "Port",
    "account": "Account",
    "base_dn": "Base DN",
    "attr_login": "Login attribute",
    "attr_firstname": "Firstname attribute",
    "attr_lastname": "Lastname attribute",
    "attr_mail": "Email attribute",
    "onthefly": "On-the-fly user creation",
    "start_date": "Start date",
    "done_ratio": "% Done",
    "auth_source": "Authentication mode",
    "hide_mail": "Hide my email address",
    "comments": "Comment",
    "url": "URL",
    "start_page": "Start page",
    "subproject": "Subproject",
    "hours": "Hours",
    "activity": "Activity",
    "spent_on": "Date",
    "identifier": "Identifier",
    "is_filter": "Used as a filter",
    "issue_to": "Related issue",
    "delay": "Delay",
    "assignable": "Issues can be assigned to this role",
    "redirect_existing_links": "Redirect existing links",
    "estimated_hours": "Estimated time",
    "column_names": "Columns",
    "time_entries": "Log time",
    "time_zone": "Time zone",
    "searchable": "Searchable",
    "default_value": "Default value",
    "comments_sorting": "Display comments",
    "parent_title": "Parent page",
    "editable": "Editable",
    "watcher": "Watcher",
    "identity_url": "OpenID URL",
    "content": "Content",
    "group_by": "Group results by",
    "sharing": "Sharing",
    "parent_issue": "Parent task",
    "member_of_group": "Assignee's group",
    "assigned_to_role": "Assignee's role",
    "text": "Text field",
    "visible": "Visible",
    "warn_on_leaving_unsaved": "Warn me when leaving a page with unsaved text",
    "issues_visibility": "Issues visibility",
    "is_private": "Private",
    "commit_logs_encoding": "Commit messages encoding",
    "scm_path_encoding": "Path encoding",
    "path_to_repository": "Path to repository",
    "root_directory": "Root directory",
    "cvsroot": "CVSROOT",
    "cvs_module": "Module",
    "repository_is_default": "Main repository",
    "multiple": "Multiple values",
    "auth_source_ldap_filter": "LDAP filter",
    "core_fields": "Standard fields",
    "timeout": "Timeout (in seconds)",
    "board_parent": "Parent forum",
    "private_notes": "Private notes",
    "inherit_members": "Inherit members",
    "generate_password": "Generate password",
    "must_change_passwd": "Must change password at next logon",
    "default_status": "Default status",
    "users_visibility": "Users visibility",
    "time_entries_visibility": "Time logs visibility",
    "total_estimated_hours": "Total estimated time",
    "default_version": "Default version",
    "remote_ip": "IP address",
    "textarea_font": "Font used for text areas",
    "updated_by": "Updated by",
    "last_updated_by": "Last updated by",
    "full_width_layout": "Full width layout",
    "digest": "Checksum",
    "default_assigned_to": "Default assignee",
    "checklist": "Checklist",
}


class Formatter:
    """
    Format bits of redmine data
    """
    def __init__(self, redmine, private=False):
        self.redmine = redmine
        self.private = private

    def annotate_issue(self, issue: Dict[Any, any]):
        """
        Annotate an issue with extra information useful to render it
        """
        for rel in issue.get("relations", []):
            rel["issue"] = self.redmine.issues.issue(rel["issue_id"])
            rel["issue_to"] = self.redmine.issues.issue(rel["issue_to_id"])
            if rel["issue_id"] != issue["id"]:
                rel["other"] = rel["issue"] or rel["issue_id"]
                rel["label"] = RELATION_NAMES[rel["relation_type"]][1]
            else:
                rel["other"] = rel["issue_to"] or rel["issue_to_id"]
                rel["label"] = RELATION_NAMES[rel["relation_type"]][0]

        for idx, event in enumerate(issue.get("journals", []), start=1):
            event["index"] = idx
            for change in event['details']:
                change["label"] = self.format_journal_detail(issue, change)
                if change["property"] == 'relation':
                    other_issue = {}
                    new_value = change.get("new_value")
                    old_value = change.get("old_value")
                    if new_value:
                        other_issue = self.redmine.issues.issue(int(new_value))
                        if other_issue and other_issue.get("is_private"):
                            change['private_issue'] = True
                    elif old_value:
                        other_issue = self.redmine.issues.issue(int(old_value))
                        if other_issue and other_issue.get("is_private"):
                            change['private_issue'] = True

    def field_label(self, name):
        """
        Return the label for a field name
        """
        if name.endswith("_id"):
            return FIELD_LABELS[name[:-3]]
        else:
            return FIELD_LABELS[name]

    def yes_no_none(self, val):
        if not val:
            return None
        elif val == "0":
            return "No"
        else:
            return "Yes"

    def format_date(self, date):
        return date

    def format_project(self, project, default=str):
        if isinstance(project, (int, str)):
            project_id = int(project)
            project = self.redmine.projects.project(project_id)
            if project is None:
                return default(project_id)
        return project["name"]

    def format_status(self, status, default=str):
        if isinstance(status, (int, str)):
            status_id = int(status)
            status = self.redmine.issues.status(status_id)
            if status is None:
                return default(status_id)
        return status["name"]

    def format_tracker(self, tracker, default=str):
        if isinstance(tracker, (int, str)):
            tracker_id = int(tracker)
            tracker = self.redmine.tracker(tracker_id)
            if tracker is None:
                print("tracker unknown")
                return default(tracker_id)
        return tracker["name"]

    def format_user(self, user: Union[int, Dict[Any, Any]], default=lambda u: "Anonymous"):
        if isinstance(user, (int, str)):
            user_id = int(user)
            user = self.redmine.user_or_group(user_id)
            if user is None:
                return default(user)
        label = user.get("login")
        if label is not None:
            return label
        return user["name"]

    def format_priority(self, priority, default=str):
        if isinstance(priority, (int, str)):
            priority_id = int(priority)
            priority = self.redmine.priority(priority_id)
            if priority is None:
                return default(priority_id)
        return priority["name"]

    def format_category(self, category, default=str):
        if isinstance(category, (int, str)):
            category_id = int(category)
            category = self.redmine.category(category_id)
            if category is None:
                return default(category_id)
        return category["name"]

    def format_version(self, version, default=str):
        if isinstance(version, (int, str)):
            version_id = int(version)
            version = self.redmine.version(version_id)
            if version is None:
                return default(version_id)
        return version["name"]

    def format_issue(self, issue, with_subject=False):
        if isinstance(issue, (int, str)):
            return f"#{issue}"
        else:
            return f"#{issue['id']}"

    def format_attachment(self, attachment: Dict[Any, Any]):
        """
        attachment is a dict containing at least {
            "id": id,
            "filename": str,
            "content_url": url,
        }
        """
        return attachment["filename"]

    def format_custom_field(self, cf: Dict[Any, Any], default=operator.itemgetter("value")):
        """
        cf is a dict containing at least {
            "id": custom field id
            "value": custom field value
        }
        """
        cf_info = self.redmine.custom_field(int(cf["id"]))
        for pv in cf_info.get("possible_values", ()):
            if pv["value"] == cf["value"] or str(pv["value"]) == cf["value"]:
                return pv["label"]
        return default(cf)

    def format_field_value(self, issue, field_name, value):
        if value is None:
            return None
        value = int(value)

        if field_name.endswith("_id"):
            field_name = field_name[:-3]

        if field_name == "project":
            return self.format_project(value)
        elif field_name == "status":
            return self.format_status(value)
        elif field_name == "tracker":
            return self.format_tracker(value)
        elif field_name == "assigned_to":
            return self.format_user(value)
        elif field_name == "priority":
            return self.format_priority(value)
        elif field_name == "category":
            return self.format_category(value)
        elif field_name == "fixed_version":
            return self.format_version(value)
        else:
            raise NotImplementedError(f"cannot find value for field {field_name}")

    def format_journal_value_updated(self, what):
        return f"{what} updated"

    def format_journal_value_changed(self, what, old_value, new_value):
        return f"{what} changed from {old_value} to {new_value}"

    def format_journal_value_set(self, what, new_value):
        return f"{what} set to {new_value}"

    def format_journal_value_added(self, what, new_value):
        return f"{what} {new_value} added"

    def format_journal_value_deleted(self, what, old_value):
        return f"{what} deleted ({old_value})"

    def format_journal_detail(self, issue, detail, html=True, only_path=False):
        """
        Returns the textual representation of a single journal detail
        """
        hide_values = False
        label = detail["name"]
        new_value = detail.get("new_value")
        old_value = detail.get("old_value")

        if detail["property"] == "attr":
            name = detail["name"]
            label = self.field_label(name)

            if name in ('due_date', 'start_date'):
                new_value = self.format_date(new_value)
                old_value = self.format_date(old_value)
            elif name in ('project_id', 'status_id', 'tracker_id', 'assigned_to_id',
                          'priority_id', 'category_id', 'fixed_version_id'):
                new_value = self.format_field_value(issue, name, new_value)
                old_value = self.format_field_value(issue, name, old_value)
            elif name == 'estimated_hours':
                new_value = str(int(float(new_value))) + " h" if new_value else None
                old_value = str(int(float(old_value))) + " h" if old_value else None
            elif name == 'parent_id':
                label = FIELD_LABELS["parent_issue"]
                if new_value:
                    new_value = self.format_issue(new_value)

                if old_value:
                    old_value = self.format_issue(old_value)
            elif name == 'is_private':
                new_value = self.yes_no_none(new_value)
                old_value = self.yes_no_none(old_value)
            elif name == 'description':
                hide_values = True
        elif detail["property"] == 'cf':
            custom_field = self.redmine.custom_field(int(detail["name"]))
            label = custom_field["name"]
            if new_value:
                new_value = self.format_custom_field({
                    "id": int(detail["name"]),
                    "value": new_value,
                })
            if old_value:
                old_value = self.format_custom_field({
                    "id": int(detail["name"]),
                    "value": old_value,
                })
        elif detail["property"] == 'attachment':
            from urllib.parse import quote
            label = "File"
            if new_value:
                new_value = self.format_attachment({
                    "id": int(detail["name"]),
                    "filename": new_value,
                    "content_url":
                        self.redmine.instance_url + "/attachments/download/{}/{}".format(
                            int(detail["name"]), quote(new_value))
                })
        elif detail["property"] == 'relation':
            if new_value and not old_value:
                new_value = self.format_issue(int(new_value), with_subject=True)
                label_dir = 0
            elif old_value and not new_value:
                old_value = self.format_issue(int(old_value), with_subject=True)
                label_dir = 1
            label = RELATION_NAMES[detail["name"]][label_dir].lower()

        if hide_values:
            # TODO: potentially embed the old and new values (possibly long) in
            # title tags
            return self.format_journal_value_updated(label)
        elif new_value:
            if detail["property"] in ("attr", "cf"):
                if old_value:
                    return self.format_journal_value_changed(label, old_value, new_value)
                # elif multiple:
                #     return f"{label} {new_value} added"
                else:
                    return self.format_journal_value_set(label, new_value)
            elif detail["property"] in ('attachment', 'relation'):
                return self.format_journal_value_added(label, new_value)
        else:
            return self.format_journal_value_deleted(label, old_value)

    def resolve_image_url(self, issue: Dict[str, Any], url: str):
        if url.startswith("http"):
            return url
        elif url.startswith("file://"):
            log.warning(f"#%d: found file: image link %r", issue['id'], url)
            return url
        elif url.startswith('"$":'):
            return url[4:]
        else:
            # The following attachments were not found in the issue,
            # probably because of user error, so we fix them manually
            # here.
            if url == "Screenshot_tails-experimental_2014-06-10_14:12:38.cleaned.png":
                url = "Screenshot_tails-experimental_2014-06-10_14_12_38.cleaned.png"
            elif url == "./custom_syslinux_boot_menu/splash.png":
                url = "splash.png"

            for attachment in issue['attachments']:
                if attachment['filename'] == url:
                    return attachment['content_url']
            else:
                if url not in ("?", "??", "§$%@"):
                    log.warn("#%d: %r is not a valid filename for an attachment", issue['id'], url)
