import pypandoc
import re

from .gitlab import GitlabProject


# re_url = re.compile(r'(?<!["@:])https?://\S+?(?=[.),\n@>\]-]+(?:\s|$))')
re_url = re.compile(r'(?<!["@:])https?://\S+?(?=[.),\n@>\]-]*(?:\s|$))')
re_at = re.compile(r'((?<!\w)@[^@]+@(?!\w))')
re_fake_image_links = re.compile(r"![§$%@?]+!")


def textilize_url(mo):
    return f'"$":{mo.group(0)}'


def escape_fake_image_link(mo):
    return "==" + mo.group(0) + "=="


def preescape_urls(orig_text):
    edited = []
    codeblock = False
    for line in orig_text.splitlines():
        if codeblock:
            # There is only one instance in redmine of this for #7626: if it
            # gives problems, one can remove this code and fix that one
            # instance by hand
            if "</pre>" in line:
                codeblock = False
                replaced = re_url.sub(textilize_url, line)
                if replaced != line:
                    line = replaced
            edited.append(line)
            continue
        else:
            if "<pre>" in line and "</pre>" not in line:
                codeblock = True
                edited.append(line)
                continue

            mo = re_at.search(line)
            if mo:
                parts = re_at.split(line)
                changed = []
                for part in parts:
                    if part.startswith("@"):
                        changed.append(part)
                    else:
                        changed.append(re_url.sub(textilize_url, part))
                edited.append("".join(changed))
                continue

            line = re_fake_image_links.sub(escape_fake_image_link, line)

            line = re_url.sub(textilize_url, line)

            edited.append(line)
            continue
    return "\n".join(edited)


class TextileConverter:
    def __init__(self, gitlab: GitlabProject, settings):
        self.gitlab = gitlab
        self.settings = settings
        self.second_run = False

    re_attachment_url = re.compile(
            r"https?://(?:labs.riseup.net|redmine.tails.boum.org)/code/attachments/(?:download/)")

    def convert(self, text: str, private: bool) -> str:
        ''' Converts Redmine Textile formated text into Markdown formated text.'''
        text = preescape_urls(text)
        text = pypandoc.convert_text(text, 'gfm', format='textile')

        # correct links to other issues
        text = re.sub(r'\\(#[0-9]+)', r'\1', text)

        if self.second_run:
            # correct links to comments
            # (needs to be done after we can ask for the node_id
            def replaceNote(m):
                issue_id = m.group(1)
                comment_id = m.group(2)
                issue = self.gitlab.issue(issue_id)
                comments = self.gitlab.comment(issue_id)
                if not comments:
                    return "#{issue_id}#note_{comment_id}"
                try:
                    note_id = comments[int(m.group(2))-1]['id']
                except IndexError:
                    return "#{issue_id}#note_{comment_id}"
                return f"{self.settings.GITLAB_URL}/{issue['project']}/-/issues/{issue_id}#note_{note_id}"
            text = re.sub(r'#([0-9]+)\\#note-([0-9]+)', replaceNote, text)

            # Update links to other issues, as they may be live
            # in other projects
            def replaceIssue(m):
                issue_id = m.group(1)
                issue = self.gitlab.issue(issue_id)
                if issue:
                    return f"{issue['project']}#{issue_id}"
                else:
                    return f"#{issue_id}"
            text = re.sub(r'#([0-9]+)', replaceIssue, text)

        # Make propper quotes
        def replaceQuoteLevel(m):
            return '>'*(len(m.group(0))//2)
        text = re.sub(r'(\\>)+(?=\s)', replaceQuoteLevel, text)

        # remove not needed commit:
        text = re.sub(r'commit:(tails\|)?(?P<hash>[0-9a-fA-F]+)', r'\g<hash>', text)

        # replace source links
        text = re.sub(r'source:([^\s]+)(?=\s)', r'[\1](\1)', text)

        # unify attachment links from redmine
        text = self.re_attachment_url.sub(f"https://redmine.tails.boum.org/code/attachments/download/", text)

        return text
