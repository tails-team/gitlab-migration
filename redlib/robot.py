from __future__ import annotations
import http.cookiejar
import mechanicalsoup
import logging
import os

log = logging.getLogger("robot")


class Robot:
    """
    Mechanized browser with functions for connecting to gitlab
    """
    def __init__(self, settings, cookiejar=None):
        self.settings = settings

        if cookiejar is None:
            cookiejar = "redlab-robot.cookies"

        # See: https://mechanicalsoup.readthedocs.io/
        self.browser = mechanicalsoup.StatefulBrowser()

        # Cookie state can be saved on a file (see Robot.save()) to preserve
        # login state across invocations
        self.cookiejar = http.cookiejar.LWPCookieJar(cookiejar)
        if os.path.exists(cookiejar):
            log.info("Loading cookies from %s", cookiejar)
            self.cookiejar.load(ignore_discard=True, ignore_expires=True)
        self.browser.set_cookiejar(self.cookiejar)

    def save(self):
        """
        Save cookie state to disk
        """
        # Save cookies, including ephemeral session cookies, to be able to
        # resume sessions across invocations
        #
        # To log out or terminate a session, just delete the cookie file
        self.cookiejar.save(ignore_discard=True, ignore_expires=True)

    def url(self, relpath):
        return f"{self.settings.GITLAB_URL}{relpath}"

    def login(self, username=None, password=None):
        """
        Log into gitlab
        """
        if username is None:
            username = "root"
        if password is None:
            import getpass
            password = getpass.getpass()

        self.browser.open(self.url("/users/sign_in"))
        self.browser.select_form("#new_user")
        self.browser["user[login]"] = username
        self.browser["user[password]"] = password
        self.browser.submit_selected()

    def logout(self):
        self.browser.open(self.url("/users/sign_out"))

    def whoami(self):
        """
        Read the name of the current user name
        """
        self.browser.open(self.url("/"))

        # page is a BeautifulSoup object.
        # See: https://www.crummy.com/software/BeautifulSoup/bs4/doc/
        page = self.browser.get_current_page()

        username = page.find("div", class_="user-name")
        if username is None:
            return None
        return username.string.strip()

    def confirm_user(self, username):
        self.browser.open(self.url(f"/admin/users/{username}"))
        for meta in self.browser.get_current_page().find_all("meta"):
            if meta.get("name") != "csrf-token":
                continue
            token = meta["content"].strip()
            break
        else:
            raise RuntimeError("Authenticity token non found in page")
        res = self.browser.session.post(self.url(f"/admin/users/{username}/confirm"), data={
            "_method": "put",
            "authenticity_token": token,
        })
        res.raise_for_status()

    def reset_password(self, email):
        self.browser.open(self.url(f"/users/password/new"))
        self.browser.select_form("#new_user")
        self.browser["user[email]"] = email
        self.browser.submit_selected()
