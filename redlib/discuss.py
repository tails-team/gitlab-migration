from __future__ import annotations
from .cmdline import Command
from .redmine import Redmine
from collections import defaultdict
import subprocess
import re


class Clique:
    def __init__(self, redmine):
        self.redmine = redmine
        self.visited = set()
        self.all = set()
        # Build a list of all valid issue IDs
        self.issue_whitelist = self.redmine.all_our_issue_ids()

    def visit(self, issue_id, depth=5):
        if depth < 0:
            return

        issue_id = int(issue_id)
        # Break loops
        if issue_id in self.visited:
            return
        self.visited.add(issue_id)

        issue = self.redmine.issues.issue(issue_id)
        if issue is None:
            return

        # Add self
        self.all.add(issue_id)

        # Visit mentions in description
        self.visit_textile(issue.get("description", ""), depth=depth-1)

        # Visit subissues
        for subissue in issue.get("children", ()):
            self.visit(subissue["id"], depth=depth-1)

        # Visit relations
        for rel in issue.get("relations", ()):
            self.visit(rel["issue_id"], depth=depth-1)
            self.visit(rel["issue_to_id"], depth=depth-1)

        # Visit mentions in journal
        for entry in issue.get("journals", ()):
            # Visit journal details
            for detail in entry.get("details", ()):
                if detail["property"] == "relation":
                    issue_id = detail.get("new_value")
                    if issue_id:
                        self.visit(issue_id, depth=depth-1)
                    issue_id = detail.get("old_value")
                    if issue_id:
                        self.visit(issue_id, depth=depth-1)

            # Visit mentions in journal notes
            notes = entry.get("notes")
            if notes is not None:
                self.visit_textile(notes, depth=depth-1)

    re_issue = re.compile(r"(?<=[^\d])#(?P<issue>\d+)")

    def visit_textile(self, text, depth):
        for line in text.splitlines():
            for mo in self.re_issue.finditer(line):
                issue_id = int(mo.group(1))
                if issue_id not in self.issue_whitelist:
                    continue
                self.visit(issue_id, depth=depth-1)


class Discuss(Command):
    """
    Extract informations useful to discuss migration issues
    """
    @classmethod
    def make_subparser(cls, subparsers):
        parser = super().make_subparser(subparsers)
        parser.add_argument("--private-notes", action="store_true",
                            help="output a list of private notes")
        parser.add_argument("--clique", action="store", nargs="+",
                            help="output a list of issue numbers and all their related issue numbers")
        parser.add_argument("--clique-depth", action="store", type=int, default=5,
                            help="maximum clique recursion depth")
        parser.add_argument("--textile", action="store_true",
                            help="look for examples of difficult textile markup")
        parser.add_argument("--list-changed-tickets", action="store", metavar="ref1[..ref2]",
                            help="show ticket numbers that changed in the git-saved cache between the two refs"
                                 " if ref2 is missing, 'master' is assumed")
        return parser

    def list_private_notes(self, redmine):
        by_user = defaultdict(list)
        project_ids = redmine.project_ids()
        for project_id in project_ids:
            issue_ids = [x["id"] for x in redmine.issues.list(project_id)]
            issue_ids.sort()
            for issue_id in issue_ids:
                issue = redmine.issues.issue(issue_id)
                for idx, entry in enumerate(issue["journals"], start=1):
                    if entry.get("private_notes"):
                        by_user[entry["user"]["name"]].append(
                                redmine.instance_url + f"/issues/{issue['id']}#note-{idx}")

        for name, entries in sorted(by_user.items()):
            print(f"{name}:")
            for entry in entries:
                print(f"  {entry}")

    def list_textile_issues(self, redmine):
        # re_pre = re.compile(r"<pre>.+?</pre>", flags=re.S)
        re_url = re.compile(r'(?<!["@:])https?://\S+?(?=[.),\n@>\]-]+(?:\s|$))')
        re_at = re.compile(r'((?<!\w)@[^@]+@(?!\w))')
        re_img = re.compile(r'(!(?:{[^}]*})?>?[^ (!]+(?:\([^)]+\))?!(?::\S+)?)')

        def check(orig_text, where):
            # Remove code blocks
            from termcolor import colored

            def color_at(mo):
                return colored(mo.group(0), 'yellow')

            def color_url(mo):
                return colored(mo.group(0), 'red')

            def escape_url(mo):
                return colored(f'"$":{mo.group(0)}', 'green')

            edited = []
            codeblock = False
            for line in orig_text.splitlines():
                if codeblock:
                    if "</pre>" in line:
                        codeblock = False
                        replaced = re_url.sub(escape_url, line)
                        if replaced != line:
                            # orig = re_url.sub(color_url, line)
                            # print(f"{where}: {orig}")
                            # print(f"{where}: {replaced}")
                            line = replaced
                    edited.append(line)
                    continue
                else:
                    if "<pre>" in line and "</pre>" not in line:
                        codeblock = True
                        edited.append(line)
                        if re_url.search(line):
                            print(f"http in <pre> line {colored(repr(line), 'red')} in {where}")
                        continue

                    mo = re_at.search(line)
                    if mo:
                        parts = re_at.split(line)
                        changed = []
                        for part in parts:
                            if part.startswith("@"):
                                changed.append(part)
                            else:
                                changed.append(re_url.sub(escape_url, part))
                        replaced = "".join(changed)
                        if replaced != line:
                            line = re_at.sub(color_at, line)
                            line = re_url.sub(color_url, line)
                            replaced = re_at.sub(color_at, replaced)
                            # print(f"{where}: {line}")
                            # print(f"{where}: {replaced}")
                        edited.append(replaced)
                        continue

                    mo = re_img.search(line)
                    if mo:
                        print(f"{where}: {line}")

                    replaced = re_url.sub(escape_url, line)
                    # if replaced != line:
                    #     orig = re_url.sub(color_url, line)
                    #     print(f"{where}: {orig}")
                    #     print(f"{where}: {replaced}")
                    edited.append(replaced)
                    continue

        project_ids = redmine.project_ids()
        for project_id in project_ids:
            issue_ids = [x["id"] for x in redmine.issues.list(project_id)]
            issue_ids.sort()
            for issue_id in issue_ids:
                issue = redmine.issues.issue(issue_id)
                check(issue["description"], f"issue #{issue_id}: description")
                for idx, entry in enumerate(issue["journals"], start=1):
                    notes = entry.get("notes")
                    if not notes:
                        continue
                    check(notes, f"issue #{issue_id}: entry notes")

    def list_changed_tickets(self, ref_range: str):
        if ".." not in ref_range:
            ref_range += "..master"

        re_issue = re.compile(r"redmine/issues/(?P<issue>\d+)(?:-relations)?.json$")
        res = subprocess.run(["git", "diff", ref_range, "--name-only"],
                             cwd=self.settings.WORKDIR, text=True,
                             stdout=subprocess.PIPE, check=True)
        for fname in res.stdout.splitlines():
            mo = re_issue.search(fname)
            if not mo:
                continue
            yield int(mo.group("issue"))

    def run(self):
        """
        Visit all redmine things to prefetch them locally
        """
        redmine = Redmine(self.settings)

        if self.args.private_notes:
            self.list_private_notes(redmine)
        elif self.args.clique:
            clique = Clique(redmine)
            for issue_id in (int(x) for x in self.args.clique):
                clique.visit(issue_id, depth=self.args.clique_depth)
            for issue_id in sorted(clique.all):
                print(issue_id)
        elif self.args.textile:
            self.list_textile_issues(redmine)
        elif self.args.list_changed_tickets:
            for issue in self.list_changed_tickets(self.args.list_changed_tickets):
                print(issue)
