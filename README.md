Resources
=========

* main ticket: https://redmine.tails.boum.org/code/issues/15878

* blueprint: https://tails.boum.org/blueprint/GitLab/

* private data repository:

        gitolite@d53ykjpeekuikgoq.onion:gitlab-migration-private

* GitLab configuration repository:

        gitolite@d53ykjpeekuikgoq.onion:gitlab-config

Dependencies
============

    apt install \
        python3-jinja2 \
        python3-pypandoc \
        python3-requests \
        python3-textile
