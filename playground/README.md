# gitlab playground

This is a system to quickly spin up throwaway libvirt VMs with gitlab instances
to experiment with.

```
$ ./playground --help
usage: playground [-h] [--verbose] [--debug] [--start-master] [--start name]

manage redmine/gitlab experimentation playground

optional arguments:
  -h, --help      show this help message and exit
  --verbose, -v   verbose output
  --debug         debug output
  --start-master  setup master image
  --start name    setup a named instance cloned from the master image
```

## Dependencies

```
apt install virt-manager python3-libvirt libguestfs-tools qemu-utils
```

Add your user to the `libvirt` and `libvirt-qemu` groups and relogin.

## Walkthrough


### Set up the master image

The master image is the starting point of all throwaway images.

To create it:

1. run `./playground --verbose --start-master`
2. run `virt-manager` and click on the `master-setup` VM to get console access
   to it
3. log in as `root:root` and run `./setup-gitlab.sh` to install gitlab.
4. visit the gitlab instance and setup the root account

This is a bandwidth intensive step that only needs to be run once.

As a result, you'll have gitlab setup to run as `http://gitlab.example.org`.

You can add a line like `192.168.122.191  gitlab.example.org` to your
`/etc/hosts` to access it. The IP address may vary on your system.

Shut down the master image when done. You won't need to start it again, unless
you want to make persistent changes to the playground base image.

You can delete the master VM in `virt-manager` if you want, just don't delete
the corresponding `.qcow2` file.

`playground --start-master` is idempotent, and if rerun with an existing master
image it will not overwrite it, and it will just recreate the VM if missing.


### Start a throwaway VM

A throwaway VM uses a copy-on-write `.qcow2` file based to quickly and cheaply
set up a disposable copy of the master image.

To create it:

1. run `./playground --verbose --start <name>`
2. run `virt-manager` and click on the vm `<name>` to get console access

Everything else is the same as with the master image, except that any changes
you perform in this VM do not affect the master image.

You can use `virt-manager` to delete the VM and its storage, and
recreate it or other images with `playground --start`, at will.

## Tips and links

* [Changing gitlab's external URL](https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-the-external-url-for-gitlab)
  in case you'd like to run and access multiple VMs at the same time.
* I have initially received "Gitlab is taking too much time to respond" errors. I have
  since increased the VM resources to match [gitlab hardware requirements](https://docs.gitlab.com/ee/install/requirements.html#hardware-requirements),
  and if it is not enough, one can tweak the number of unicorn workers. See
  [here](https://docs.gitlab.com/omnibus/settings/unicorn.html),
  [here](https://gitlab.com/gitlab-org/gitlab-foss/issues/30095), and
  [here](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/3632). Check also
  the unicorn error log under `/var/log/gitlab/`.
  It could also be that he VM has just been booted and gitlab hasn't fully
  started yet.
* You may want to setup API access tokens in the master image, to have them
  already available in the playground VMs.
* [Change any user's password](https://docs.gitlab.com/ee/security/reset_root_password.html)

## Troubleshooting

* If you get `Network default not found`: run `sudo virsh net-start default`

## Desirable features

It would be nice to assign static IPs to VM to make it easier to manager
`/etc/hosts`, but that would require the `playground` script to set up and
manager a custom libvirt network, which is doable, but probably overkill at
this stage.

I have found that the IPs assigned to VMs are quite stable, and I have made it
so that the VM MAC addresses are computed based on the VM names, so hopefully
one should always get the same IP for a VM with the same name.
