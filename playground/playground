#!/usr/bin/python3

import os
import logging
import subprocess
import argparse
import sys
import libvirt
import uuid
import hashlib
import contextlib

log = logging.getLogger("playground")


def get_uuid(path):
    return str(uuid.uuid3(uuid.NAMESPACE_URL, "httpd://tails.boum.org/gitlab/playground/" + path))


class Config:
    """
    Common configuration
    """
    def __init__(self, root="./data"):
        # URI to connect to libvirt
        self.libvirt_uri = "qemu:///system"

        # Root directory where images and other data are stored
        self.root = os.path.abspath(root)

        # Directory where disk images are stored
        self.image_dir = os.path.join(self.root, "images")

        # ssh public key to use to connect to the machine
        # (autodetected from user's public key)
        self.ssh_public_key = None
        for path in ("~/.ssh/id_rsa.pub",):
            path = os.path.expanduser(path)
            if os.path.exists(path):
                self.ssh_public_key = path
                break


class MasterImage:
    """
    Generate a master image with a fresh gitlab setup, that can be cloned for experiments.

    The master image will be a stock debian buster image, with root:root as
    credentials, and a setup-gitlab.sh script in /root that can be run
    manually
    """
    def __init__(self, config):
        self.config = config
        self.pathname = os.path.join(config.image_dir, "master-gitlab.qcow2")
        if not os.path.exists(self.pathname):
            self.create()

    def create(self):
        log.info("Building master image as %s", self.pathname)
        os.makedirs(self.config.image_dir, exist_ok=True)
        cmd = [
            "virt-builder", "debian-10",
            "--format", "qcow2",
            "-o", self.pathname,
            "--root-password", "password:root",
            "--firstboot", os.path.join(self.config.root, "firstboot.sh"),
            "--copy-in", os.path.join(self.config.root, "setup-gitlab.sh") + ":/root/",
            "--copy-in", os.path.join(self.config.root, "gitlab.gpg") + ":/root/",
            "--copy-in", os.path.join(self.config.root, "gitlab-ee.list") + ":/etc/apt/sources.list.d/",
        ]

        if self.config.ssh_public_key:
            log.info(" using ssh public key %s", self.config.ssh_public_key)
            cmd += ["--ssh-inject", "root:file:" + self.config.ssh_public_key]

        subprocess.run(cmd, check=True)


class Image:
    """
    Generate an image by cloning a master image
    """
    def __init__(self, config, master_image, name):
        self.config = config
        self.master_image = master_image
        self.name = name
        self.uuid = get_uuid("image/" + self.name)
        self.pathname = os.path.join(config.image_dir, self.name + ".qcow2")
        if not os.path.exists(self.pathname):
            cmd = ["qemu-img", "create", "-f", "qcow2", '-F', 'qcow2',
                   "-o", "backing_file=" + self.master_image.pathname,
                   self.pathname]
            subprocess.run(cmd, stdout=subprocess.DEVNULL, check=True)


class Network:
    """
    Bring up the network
    """
    def __init__(self, config, connection, name):
        self.name = name
        self.config = config
        self.connection = connection

    def lookup(self):
        """
        Lookup a virNetwork object for the test network.
        """
        # Iterate networks to prevent a spurious message on stderr if
        # networkLookupByName fails
        if self.name not in self.connection.listNetworks():
            return None

        try:
            return self.connection.networkLookupByName(self.name)
        except libvirt.libvirtError as e:
            if str(e).startswith("Network not found:"):
                return None
            raise

    def start(self):
        """
        Activate and return the virNetwork object for this system
        """
        # sudo virsh net-start default  (might need to be done by hand if it says 'Network default not found')
        network = self.lookup()
        if network is None:
            raise Fail("Network {} not found".format(self.name))

        if not network.isActive():
            # virsh -c qemu:///system net-start $name
            log.info("start network %s", self.name)
            network.create()

        return network


class Machine:
    """
    VM definition
    """
    def __init__(self, config, connection, name, uuid, image, network):
        self.config = config
        self.connection = connection
        self.name = name
        self.uuid = uuid
        self.network = network
        # Build a mac address based on the machine name. No particular security
        # is required from this hashing, it's just to tie mac addresses to
        # machine names.
        digest = hashlib.sha1(name.encode()).hexdigest()
        self.mac = "52:54:00:{}:{}:{}".format(digest[0:2], digest[2:4], digest[4:6])
        # virt-install --dry-run --print-xml --connect qemu:///system
        #  --ram 8192 -n fuss_manager_vm1 --os-type=linux --os-variant=debian10
        #  --network network=default --disk vol=$pool/$image,device=disk,format=qcow2
        #  --vcpus=1 --vnc --import

        with open(os.path.join(config.root, "vm.xml"), "rt") as fd:
            self.cfg = fd.read().format(name=name, uuid=uuid, image=image, network=self.network.name, mac=self.mac)

    def lookup(self):
        """
        Lookup a domain object for this domain
        """
        # Iterate domains to prevent a spurious message on stderr if
        # lookupByName fails
        for domain in self.connection.listAllDomains():
            if domain.name() == self.name:
                return domain
        return None

    def start(self):
        domain = self.lookup()
        if domain is not None:
            if domain.isActive():
                log.info("Halting existing domain %s", self.name)
                domain.destroy()
            log.info("Undefining existing domain %s", self.name)
            domain.undefine()

        log.info("Defining domain %s", self.name)
        domain = self.connection.defineXML(self.cfg)
        domain.create()
        return domain


@contextlib.contextmanager
def libvirt_connection(config):
    """
    Start/end a libvirt connection
    """
    conn = libvirt.open(config.libvirt_uri)
    try:
        yield conn
    finally:
        conn.close()


class Fail(Exception):
    pass


def main():
    parser = argparse.ArgumentParser(description="manage redmine/gitlab experimentation playground")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")
    parser.add_argument("--debug", action="store_true", help="debug output")
    parser.add_argument("--start-master", action="store_true", help="setup master image")
    parser.add_argument("--start", action="store", metavar="name",
                        help="setup a named instance cloned from the master image")
    parser.add_argument("--network-name", action="store", metavar="network_name",
                        default="default",
                        help="name of the libvirt network to use")

    args = parser.parse_args()

    # Setup logging
    FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
    if args.debug:
        logging.basicConfig(level=logging.DEBUG, stream=sys.stderr, format=FORMAT)
    elif args.verbose:
        logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)
    else:
        logging.basicConfig(level=logging.WARN, stream=sys.stderr, format=FORMAT)

    config = Config()

    if args.start_master:
        master = MasterImage(config)

        with libvirt_connection(config) as connection:
            network = Network(config, connection, args.network_name)
            network.start()

            # This UUID was generated while writing this script, and can be used to
            # identify the master-setup VM unequivocally
            setup = Machine(
                        config, connection, "master-setup",
                        get_uuid("master-image"), master.pathname,
                        network=network)
            setup.start()
            log.info("You can now login as root:root and run setup-gitlab.sh to install gitlab")
    elif args.start:
        master = MasterImage(config)
        image = Image(config, master, args.start)

        with libvirt_connection(config) as connection:
            network = Network(config, connection, args.network_name)
            network.start()

            # This UUID was generated while writing this script, and can be used to
            # identify the master-setup VM unequivocally
            setup = Machine(
                        config, connection, image.name,
                        image.uuid, image.pathname,
                        network=network)
            setup.start()
            log.info("You can now login as root:root")
    else:
        raise Fail("Please select an action")


if __name__ == "__main__":
    try:
        main()
    except Fail as e:
        print(e, file=sys.stderr)
        sys.exit(1)
    except Exception as e:
        log.exception("uncaught exception: {}".format(str(e)))
