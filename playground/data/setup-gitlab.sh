#!/bin/sh
set -uxe

# Do this before the update, because otherwise the gitlab source list cannot update
# and apt-key cannot add the key to the keyring
apt-get install apt-transport-https gpg
apt-key add gitlab.gpg

apt-get update
apt-get upgrade --with-new-pkgs -y
apt-get install -y openssh-server ca-certificates gnupg debian-archive-keyring curl

apt-get update

export EXTERNAL_URL="http://gitlab.example.org"

apt-get install gitlab-ee

apt-get clean
