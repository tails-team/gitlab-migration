#!/bin/sh

# Fixup a newly installed debian buster in libvirt

# Fix network interface name
sed -i -re 's/ens2/enp1s0/' /etc/network/interfaces

# Bring up the network
ifup enp1s0

# Set a random ssh host key
ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
systemctl restart sshd
