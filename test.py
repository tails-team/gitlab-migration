from redlib.settings import Settings
from redlib.gitlab import GitlabProject
from redlib.redmine import Redmine
from redlib.converter import Converter
from redlib import textileconverter

import logging

log = logging.getLogger()

settings = Settings()
settings.load('settings-playground.py')

gitlab = GitlabProject(settings)
redmine = Redmine(settings)
converter = Converter(redmine, gitlab, settings)
conv = textileconverter.TextileConverter(gitlab, settings)
